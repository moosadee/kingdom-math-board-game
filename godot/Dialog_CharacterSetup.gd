extends MarginContainer

var whichPlayer = 1
var totalPlayers

func _ready():
	print( "Dialog_CharacterSetup::_ready()" )
	# Called when the node is added to the scene for the first time.
	# Initialization here
		
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func setup_screen( total ):
	print( "Dialog_CharacterSetup::setup_screen()" )
	totalPlayers = total
	setup_player_editor( 1 )

func setup_player_editor( nextPlayer ):
	whichPlayer = nextPlayer
	$VBoxContainer/Label_WhichPlayer.text = "Player " + str( whichPlayer )

func _on_Button_Next_pressed():
	if whichPlayer == totalPlayers: 
		# done
		print( "done" )
	else:
		setup_player_editor( whichPlayer + 1 )