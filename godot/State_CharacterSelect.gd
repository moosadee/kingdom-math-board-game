extends Node
export (int) var totalPlayers

func _ready():
	print( "State_CharacterSelect::_ready()" )
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func select_player_count( count ):
	print( "Show Character Setup" )
	totalPlayers = count
	print( "Total players: ", totalPlayers )
	$Dialog_HowManyPlayers.hide()
	$Dialog_CharacterSetup.show()
	$Dialog_CharacterSetup.setup_screen( totalPlayers )

func _on_State_CharacterSelect_total_players_is_1():
	print( "State_CharacterSelect::_on_State_CharacterSelect_total_players_is_1" )
	select_player_count( 1 )

func _on_State_CharacterSelect_total_players_is_2():
	print( "State_CharacterSelect::_on_State_CharacterSelect_total_players_is_2" )
	select_player_count( 2 )

func _on_State_CharacterSelect_total_players_is_3():
	print( "State_CharacterSelect::_on_State_CharacterSelect_total_players_is_3" )
	select_player_count( 3 )

func _on_State_CharacterSelect_total_players_is_4():
	print( "State_CharacterSelect::_on_State_CharacterSelect_total_players_is_4" )
	select_player_count( 4 )
