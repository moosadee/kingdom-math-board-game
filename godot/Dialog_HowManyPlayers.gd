extends MarginContainer
signal total_players_is_1
signal total_players_is_2
signal total_players_is_3
signal total_players_is_4

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	print( "Dialog_HowManyPlayers::_ready()" )
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Button_1Player_pressed():
	print( "Emit signal: total_players_is_1" )
	emit_signal( "total_players_is_1" )

func _on_Button_2Players_pressed():
	print( "Emit signal: total_players_is_2" )
	emit_signal( "total_players_is_2" )

func _on_Button_3Players_pressed():
	print( "Emit signal: total_players_is_3" )
	emit_signal( "total_players_is_3" )

func _on_Button_4Players_pressed():
	print( "Emit signal: total_players_is_4" )
	emit_signal( "total_players_is_4" )
