#ifndef _CharacterCustomizerState
#define _CharacterCustomizerState

#include "../kuko/states/IState.hpp"
#include "../kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>
#include <stack>

class CharacterCustomizerState : public kuko::IState
{
    public:
    CharacterCustomizerState( const std::string& contentPath );
    virtual ~CharacterCustomizerState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    std::string m_state;

    std::string m_contentPath;
};

#endif
