#ifndef _TITLESTATE
#define _TITLESTATE

#include "../kuko/states/IState.hpp"
#include "../kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>
#include <stack>

class TitleState : public kuko::IState
{
    public:
    TitleState( const std::string& contentPath );
    virtual ~TitleState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    void Update_Title( const std::string& clickedButtonId );
    void Update_Play( const std::string& clickedButtonId );
    void Update_Play_Story( const std::string& clickedButtonId );
    void Update_Play_LevelSelect( const std::string& clickedButtonId );
    void Update_Options( const std::string& clickedButtonId );
    void Update_Help( const std::string& clickedButtonId );
    void Update_Help_HowToPlay( const std::string& clickedButtonId );
    void Update_Help_Credits( const std::string& clickedButtonId );
    void Update_CharacterHub( const std::string& clickedButtonId );

    void Goto_Title();
    void Goto_Play();
    void Goto_Character_Hub();
    void Goto_Play_LevelSelect( int world );
    void Goto_Options();
    void Goto_Help();
    void Goto_Help_HowToPlay();
    void Goto_Help_Credits();
    void GoBack();

    private:
    kuko::MenuManager menuManager;

    void SetupMenu_TitleScreen();
    void SetupMenu_OptionsScreen();
    void SetupMenu_HelpScreen();
    void SetupMenu_CharacterHub();
    void SetupMenu_LevelSelectScreen();

    void AdjustVolume( const std::string& buttonName );

    std::string m_state;
    std::stack< std::string > m_stateStack;
    std::vector< std::string > m_lstSaveGames;

    std::string m_contentPath;

    const int TOTAL_SAVES = 8;
};

#endif
