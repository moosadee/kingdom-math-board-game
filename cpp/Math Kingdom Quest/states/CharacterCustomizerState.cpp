#include "CharacterCustomizerState.hpp"
#include <cstdio>
#include <cstdlib>

#include "../kuko/managers/ImageManager.hpp"
#include "../kuko/managers/InputManager.hpp"
#include "../kuko/managers/FontManager.hpp"
#include "../kuko/managers/LanguageManager.hpp"
#include "../kuko/managers/SoundManager.hpp"
#include "../kuko/managers/ConfigManager.hpp"
#include "../kuko/utilities/Logger.hpp"
#include "../kuko/base/Application.hpp"
#include "../kuko/utilities/StringUtil.hpp"
#include "../kuko/utilities/Platform.hpp"
#include "../kuko/utilities/Messager.hpp"

CharacterCustomizerState::~CharacterCustomizerState()
{
}

CharacterCustomizerState::CharacterCustomizerState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void CharacterCustomizerState::Setup()
{
    Logger::Out( "Setup", "CharacterCustomizerState::Setup" );

    m_state = "";
    kuko::IState::Setup();

    menuManager.LoadMenuScript( m_contentPath + "menus/character_customizer.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}

void CharacterCustomizerState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void CharacterCustomizerState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void CharacterCustomizerState::Update()
{
    menuManager.Update();
}

void CharacterCustomizerState::Draw()
{
    menuManager.Draw();
}
