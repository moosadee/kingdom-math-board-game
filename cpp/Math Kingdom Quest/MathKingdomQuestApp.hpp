#ifndef MKQ_HPP
#define MKQ_HPP

#include "kuko/base/Application.hpp"
#include "kuko/utilities/Logger.hpp"
#include "kuko/utilities/StringUtil.hpp"
#include "kuko/managers/ImageManager.hpp"
#include "kuko/managers/StateManager.hpp"
#include "kuko/managers/InputManager.hpp"
#include "kuko/managers/FontManager.hpp"
#include "kuko/managers/LanguageManager.hpp"
#include "kuko/managers/LuaManager.hpp"
#include "kuko/managers/SoundManager.hpp"
#include "kuko/managers/ConfigManager.hpp"

#include <string>

class MathKingdomQuestApp
{
    public:
    MathKingdomQuestApp();
    ~MathKingdomQuestApp();

    void Run();
    void Cleanup();

    private:
    void Setup();
    void KukoSetup();
    void AppSetup();

    std::string m_contentPath;
    bool m_goToLanguageSelect;

    kuko::StateManager m_stateManager;

    static std::string GetGotoLevel();
    static void SetGotoLevel( const std::string& level );

    private:
    static std::string m_gotoLevel;
};

#endif
