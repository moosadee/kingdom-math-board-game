#include "MathKingdomQuestApp.hpp"

int main( int argc, char* args[] )
{
    MathKingdomQuestApp app;
    app.Run();

    return 0;
}
