assets = {
    { name = "bg_world1",           path = "content/graphics/ui/worlds/background_world1.png" },
    { name = "button_left",         path = "content/graphics/ui/button_nav_left.png" },
    { name = "button_question",     path = "content/graphics/ui/btn_question.png" },
    { name = "button_level",        path = "content/graphics/ui/worlds/world_region.png" },
    { name = "button_cutscene",     path = "content/graphics/ui/worlds/world_cutscene.png" },
}

menu_options = {
    total_pages = 5
}

buttonw = 75
buttonh = 75

txtx = 16
txty = 16

elements = {
    { ui_type = "image",  id = "background", texture_id = "bg_world1",   x = 0, y = 0,   width = scrw, height = scrh },

    -- Level select: World 1
    { page = 1, ui_type = "button", id = "btn_back",   texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },
    { page = 1, ui_type = "button", id = "btn_help",   texture_id = "button_question", x = 10, y = 10, width = 107, height = 107 },

    { page = 1, ui_type = "button",     id = "btn_w1_cut", texture_id = "button_cutscene", x = 250, y = 60, width = buttonw, height = buttonh },

    { page = 1, ui_type = "button",     id = "playlevel_w1_l1", texture_id = "button_level", x = 450, y = 45, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l2", texture_id = "button_level", x = 325, y = 300, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l3", texture_id = "button_level", x = 630, y = 245, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l4", texture_id = "button_level", x = 810, y = 420, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l5", texture_id = "button_level", x = 1065, y = 245, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l6", texture_id = "button_level", x = 920, y = 90, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button",     id = "playlevel_w1_l7", texture_id = "button_level", x = 1040, y = 580, width = buttonw, height = buttonh },


    { page = 1, ui_type = "label", id = "lbl_w1_l1", text = "1-1", font_id = "main", x = 450+txtx, y = 45+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,  use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l2", text = "1-2", font_id = "main", x = 325+txtx, y = 300+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,  use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l3", text = "1-3", font_id = "main", x = 630+txtx, y = 245+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,  use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l4", text = "1-4", font_id = "main", x = 810+txtx, y = 420+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,  use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l5", text = "1-5", font_id = "main", x = 1065+txtx, y = 245+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,   use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l6", text = "1-6", font_id = "main", x = 920+txtx, y = 90+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255,  use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
    { page = 1, ui_type = "label", id = "lbl_w1_l7", text = "1-7", font_id = "main", x = 1040+txtx, y = 580+txty, width = 0, height = 40, font_r = 122, font_g = 89, font_b = 18, font_a = 255, use_shadow = 1, shadow_r = 224, shadow_b = 134, shadow_g = 196, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2 },
}
