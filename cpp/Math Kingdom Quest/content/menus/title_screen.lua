assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
    { name = "logo",           path = "content/graphics/ui/title_logo.png" },
    { name = "title_text",     path = "content/graphics/ui/mainmenu_title.png" },
    { name = "button_main",    path = "content/graphics/ui/button_main.png" },
    { name = "button_sub",     path = "content/graphics/ui/button_sub.png" },
    { name = "button_fb",      path = "content/graphics/ui/button_social_facebook.png" },
    { name = "button_tw",      path = "content/graphics/ui/button_social_twitter.png" },
    { name = "button_left",    path = "content/graphics/ui/button_nav_left.png" },
    { name = "icons",          path = "content/graphics/ui/menu_icons.png" },
    { name = "missing_in_build",    path = "content/graphics/ui/missing_in_build.png" },
    { name = "locked",         path = "content/graphics/ui/locked.png" }
}

menu_options = {
    total_pages = 3
}

button1y = 45
button2y = 220
button3y = 395
button1inc = 95

buttonx = 20
buttonx2 = 770
col2X = 760
scrw = 1280
scrh = 720

buttonw = 543
buttonh = 71
textw = 543
texth = 50

shadowr = 163
shadowg = 24
shadowb = 24

function GetColX( index )
    if ( index == nil ) then
        index = 0
    end

    return buttonx+10 + (index * 50)
end

function GetRowY( index )
    return index * button1inc + button1y
end

function ButtonText( newId, newText, row, col )
    return {
            page = 1,
            ui_type = "label",
            id = newId,
            text_id = newText,
            x = GetColX( col ) + 100, y = GetRowY( row ) + 10,
            width = 0,
            height = texth,
            font_id = "main",
            font_r = 255, font_g = 255, font_b = 255, font_a = 255,
            shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1
        }
end

function ButtonBody( newId, row, col )
    return {
        page = 1,
        ui_type = "button",
        id = newId,
        texture_id = "button_main",
        x = GetColX( col ), y = GetRowY( row ),
        width = buttonw, height = buttonh
    }
end

function ButtonIcon( newId, icon, row, col )
    return {
        page = 1,
        ui_type = "image",
        id = newId,
        texture_id = "icons",
        x = GetColX( col ) + 20,
        y = GetRowY( row ) + 12,
        width = 45,
        height = 45,
        frame_x = icon * 45,
        frame_y = 0,
        frame_width = 45,
        frame_height = 45
    }
end

-- TODO: Make sure buttons w/ labels work??
elements = {
    -- Common
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    { ui_type = "image", id = "logo", texture_id = "logo",         x = col2X-35,  y = 380,  width = 330, height = 300,   effect = "", effect_speed = 50 },
    { ui_type = "image", id = "ctitle_text", texture_id = "title_text",   x = col2X,     y = 20,   width = 400, height = 400,   effect = "",    effect_speed = 50 },

    -- Page 1: Title Screen
    { page = 1, ui_type = "button", id = "btn_moosader",    texture_id = "button_sub",     x = buttonx2+40, y = scrh-110,       width = 250, height = 66 },
    { page = 1, ui_type = "button", id = "btn_fb",          texture_id = "button_fb",      x = buttonx2+320, y = scrh-110,   width = 70, height = 70 },
    { page = 1, ui_type = "button", id = "btn_tw",          texture_id = "button_tw",      x = buttonx2+420, y = scrh-110,   width = 70, height = 70 },

    -- Whee
    { page = 1, ui_type = "label", id = "lbl_moose", text_id = "mainmenu_moremoose", x = buttonx2+50, y = scrh-100,  width = 250, height = 40, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Page 2: Options Menu

    -- Page 3: Play Menu
    { page = 3, ui_type = "button", id = "btn_story",  texture_id = "button_main", x = buttonx, y = button1y, width = buttonw, height = buttonh },
    { page = 3, ui_type = "button", id = "btn_editor", texture_id = "button_main", x = buttonx, y = button2y, width = buttonw, height = buttonh },
    { page = 3, ui_type = "button", id = "btn_custom", texture_id = "button_main", x = buttonx, y = button3y, width = buttonw, height = buttonh },
    { page = 3, ui_type = "button", id = "btn_back",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 3, ui_type = "label", id = "lbl_story", text_id = "playmenu_story", x = buttonx+10, y = button1y-5, width = 0, height = texth, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "lbl_editor", text_id = "playmenu_editor", x = buttonx+10, y = button2y-5, width = 0, height = texth, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "lbl_custom", text_id = "playmenu_custom", x = buttonx+10, y = button3y-5,  width = 0, height = texth, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
}

-- Main menu buttons
table.insert( elements, ButtonText( "lbl_creator", "mainmenu_creator", 0 ) )
table.insert( elements, ButtonBody( "btn_creator", 0 ) )
table.insert( elements, ButtonIcon( "img_creator", 0, 0 ) )

table.insert( elements, ButtonText( "lbl_play", "mainmenu_play", 1 ) )
table.insert( elements, ButtonBody( "btn_play", 1 ) )
table.insert( elements, ButtonIcon( "img_play", 1, 1 ) )

--table.insert( elements, ButtonText( "lbl_boardgame", "mainmenu_boardgame", 2, 1 ) )
--table.insert( elements, ButtonBody( "btn_boardgame", 2, 1 ) )
--
--table.insert( elements, ButtonText( "lbl_dungeon", "mainmenu_dungeon", 3, 1 ) )
--table.insert( elements, ButtonBody( "btn_dungeon", 3, 1 ) )

table.insert( elements, ButtonText( "lbl_options", "mainmenu_options", 2 ) )
table.insert( elements, ButtonBody( "btn_options", 2 ) )
table.insert( elements, ButtonIcon( "img_options", 2, 2 ) )

table.insert( elements, ButtonText( "lbl_help", "mainmenu_help", 3 ) )
table.insert( elements, ButtonBody( "btn_help", 3 ) )
table.insert( elements, ButtonIcon( "img_help", 3, 3 ) )

table.insert( elements, ButtonText( "lbl_teacherinfo", "mainmenu_teacherinfo", 4 ) )
table.insert( elements, ButtonBody( "btn_teacherinfo", 4 ) )
table.insert( elements, ButtonIcon( "img_teacherinfo", 4, 4 ) )

table.insert( elements, ButtonText( "lbl_exit", "mainmenu_exit", 6 ) )
table.insert( elements, ButtonBody( "btn_exit", 6 ) )
table.insert( elements, ButtonIcon( "img_exit", 5, 6 ) )

