assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
    { name = "hero_bg",           path = "content/graphics/ui/bg_hero.png" },
    { name = "button_main",    path = "content/graphics/ui/button_main.png" },
    { name = "button_sub",     path = "content/graphics/ui/button_sub.png" },
    { name = "button_fb",      path = "content/graphics/ui/button_social_facebook.png" },
    { name = "button_tw",      path = "content/graphics/ui/button_social_twitter.png" },
    { name = "button_left",    path = "content/graphics/ui/button_nav_left.png" },
    { name = "locked",         path = "content/graphics/ui/locked.png" }
}

menu_options = {
    total_pages = 1
}

selectorWH = 250
buttonx = 30
buttony = 20
scrw = 1280
scrh = 720
buttonIncX = selectorWH + 73
buttonIncY = selectorWH + 30

shadowr = 163
shadowg = 24
shadowb = 24
textw = 543
nameTextH = 25
textOffsetX = 10
textOffsetY = selectorWH - 60

elements = {
    -- Common
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    { page = 1, ui_type = "button", id = "btn_back1",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 1, ui_type = "button",
        id = "btn_hero1",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 0), y = buttony,
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero1",
        text = "NAME1",
        x = buttonx + (buttonIncX * 0) + textOffsetX, y = buttony + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero2",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 1), y = buttony,
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero2",
        text = "NAME2",
        x = buttonx + (buttonIncX * 1) + textOffsetX, y = buttony + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero3",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 2), y = buttony,
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero3",
        text = "NAME3",
        x = buttonx + (buttonIncX * 2) + textOffsetX, y = buttony + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero4",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 3), y = buttony,
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero4",
        text = "NAME4",
        x = buttonx + (buttonIncX * 3) + textOffsetX, y = buttony + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero5",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 0), y = buttony + (buttonIncY * 1),
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero5",
        text = "NAME5",
        x = buttonx + (buttonIncX * 0) + textOffsetX, y = buttony + (buttonIncY * 1) + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero6",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 1), y = buttony + (buttonIncY * 1),
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero6",
        text = "NAME6",
        x = buttonx + (buttonIncX * 1) + textOffsetX, y = buttony + (buttonIncY * 1) + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero7",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 2), y = buttony + (buttonIncY * 1),
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero7",
        text = "NAME7",
        x = buttonx + (buttonIncX * 2) + textOffsetX, y = buttony + (buttonIncY * 1) + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button",
        id = "btn_hero8",
        texture_id = "hero_bg",
        x = buttonx + (buttonIncX * 3), y = buttony + (buttonIncY * 1),
        width = selectorWH, height = selectorWH },
    { page = 1, ui_type = "label",
        id = "lbl_hero8",
        text = "NAME8",
        x = buttonx + (buttonIncX * 3) + textOffsetX, y = buttony + (buttonIncY * 1) + textOffsetY,
        width = 0, height = nameTextH,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main",
        shadow_r = shadowr, shadow_g = shadowg, shadow_b = shadowb, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

}
