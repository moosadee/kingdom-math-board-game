assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
}

elements = {
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh }
}
