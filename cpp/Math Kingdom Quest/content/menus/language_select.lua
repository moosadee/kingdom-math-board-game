assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_options.png" },
    { name = "button",        path = "content/graphics/ui/button_bubble.png" }
}

buttonX = 20
buttonY = 100
inc = 100

textHeight = 40
buttonWidth = 75
half = 1280/2

elements = {
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = 1280, height = 720 },

    { ui_type = "button", id = "btn_english", texture_id = "button", x = buttonX, y = buttonY, width = buttonWidth, height = buttonWidth },
    { ui_type = "label",  id = "lbl_english", text_id = "language_english", x = buttonX+125, y = buttonY+10, width = 0, height = textHeight, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { ui_type = "button", id = "btn_spanish", texture_id = "button", x = buttonX, y = buttonY+inc, width = buttonWidth, height = buttonWidth },
    { ui_type = "label",  id = "lbl_spanish", text_id = "language_spanish", x = buttonX+125, y = buttonY+inc+10, width = 0, height = textHeight, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

-- Conlangs

    { ui_type = "button", id = "btn_esperanto", texture_id = "button", x = buttonX+half, y = buttonY+inc*0, width = buttonWidth, height = buttonWidth },
    { ui_type = "label",  id = "lbl_esperanto", text_id = "language_esperanto", x = buttonX+125+half, y = buttonY+inc*0+10, width = 0, height = textHeight, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { ui_type = "button", id = "btn_ido", texture_id = "button", x = buttonX+half, y = buttonY+inc*1, width = buttonWidth, height = buttonWidth },
    { ui_type = "label",  id = "lbl_ido", text_id = "language_ido", x = buttonX+125+half, y = buttonY+inc*1+10, width = 0, height = textHeight, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { ui_type = "button", id = "btn_interlingua", texture_id = "button", x = buttonX+half, y = buttonY+inc*2, width = buttonWidth, height = buttonWidth },
    { ui_type = "label",  id = "lbl_interlingua", text_id = "language_interlingua", x = buttonX+125+half, y = buttonY+inc*2+10, width = 0, height = textHeight, font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
}
