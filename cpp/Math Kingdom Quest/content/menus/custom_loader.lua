assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
    { name = "button_main",    path = "content/graphics/ui/button_main.png" },
    { name = "button_left",    path = "content/graphics/ui/button_nav_left.png" },
    { name = "button_edit",    path = "content/graphics/ui/button_edit.png" }
}

scrw = 1280
scrh = 720
buttonh = 50

elements = {
    -- Common
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    { page = 1, ui_type = "label", id = "lbl_levels",
        text_id = "playmenu_custom",
        x = 20, y = 10, centered = 1,
        width = 0, height = buttonh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255,
        shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },

    { ui_type = "button", id = "btn_back",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },
}
