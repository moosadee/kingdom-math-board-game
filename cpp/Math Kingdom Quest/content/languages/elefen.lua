if ( language == nil ) then language = {} end

language = {
	suggested_font = "NotoSans-Bold.ttf",

	language = "Elefen",

	game_title = "Fin 'n' Kit",
	game_creator = "Moosader LLC",
	game_website = "Moosader.com",
	game_year = "2016",
	game_version = "Dev Version 2016-08-09",

	game_title_1 = "Fin",
	game_title_2 = "'n'",
	game_title_3 = "Kit",

	mainmenu_play = "Jua",
	mainmenu_exit = "Sorti",
	mainmenu_options = "Preferes",
	mainmenu_help = "Aida",
	mainmenu_moremoose = "Plu Moosader!",

	menu_tiles = "Telias",
	menu_items = "Ojetos",
	menu_select = "Eleje",
	menu_options2 = "Preferes",

	playmenu_story = "Modo nara",
	playmenu_editor = "Editador de niveles",
	playmenu_custom = "Niveles par comanda",

    world_1 = "Mundo 1",
    world_2 = "Mundo 2",
    world_3 = "Mundo 3",
    world_4 = "Mundo 4",
    world_5 = "Mundo 5",

    world_1_name = "Plaia Plasente",
    world_2_name = "Mundo Misteriosa",
    world_3_name = "Mundo Misteriosa",
    world_4_name = "Mundo Misteriosa",
    world_5_name = "Mundo Misteriosa",

    help_how_to_play = "Como jua",
    help_credits = "Titulos",

    options_sound_volume = "Volum de sona",
    options_music_volume = "Volum de musica",
    options_language = "Cambia lingua",

	editor_map_name = "Nom de mapa",
	editor_save_map = "Fisa",
	editor_saved = "Fisada: ",
	editor_load_map = "Carga",
	editor_load_map_name = "default.fin",
	editor_map_properties = "Proprias",
	editor_file_mgmt = "Maneja de fixes",
	editor_clear_map = "Vacui la mapa",
	editor_back = "Sorti",
	editor_confirm_exit = "Abandona la editador",
	editor_theme = "Tema",
	editor_theme_afternoon = "Posmedia",
	editor_theme_sunset = "Reposa de sol",
    editor_trinkets = "Ornetas",
    editor_obstacles = "Ostaculos",
    editor_misc_tiles = "Telias variosas",
    editor_extra_life = "Colpa ajuntable",
    editor_level_end = "Fin de nivel",
    editor_filename = "Nom de fix",
    editor_background = "Fondo",
    editor_ground = "Tera",
    editor_scroll = "Rapidia de rola",
    editor_clear_map = "Vacui la mapa",
    editor_extra_life = "Vive ajuntable",
    editor_level_end = "Marca de fin de nivel",
    editor_test_level = "Proba",
    editor_music = "Musica",
    editor_clear_level = "Vacuida",
    editor_behavior = "Condui",
    editor_scale = "Scala",
    editor_song_1 = "Canta 1",
    editor_song_2 = "Canta 2",
    editor_song_3 = "Canta 3",
    editor_scale_50 = "50%",
    editor_scale_100 = "100%",
    editor_scale_150 = "150%",
    editor_status_hello = "Alo!",
    editor_status_set_default_behavior = "La condui costumal ia es cambiada",
    editor_status_set_default_scale = "La scala costumal ia es cambiada",
    editor_status_set_tile_behavior = "Condui: ",
    editor_status_set_tile_scale = "Scala cambiada: ",
    editor_status_added_new_tile = "Telia ajuntada: ",
    editor_status_removed_tile = "Telia sutraeda",
    editor_status_tile_count = "Soma de telias: ",
    editor_status_current_brush = "Brosa corente: ",
    editor_status_updated_background = "La fundo ia es corentida",
    editor_status_updated_ground = "La tera ia es corentida",
    editor_status_updated_song = "La canta ia es corentida",
    editor_status_updated_speed = "La rapidia de rola ia es corentida",

    game_menu_success = "Susede!",
    game_menu_failure = "Fali!",
    game_menu_paused = "Pausada",
    game_menu_exit = "Sorti",
    game_menu_restart = "Recomensa",
    game_menu_continue = "Continua",
    game_menu_next_level = "Seguente nivel",

    credits_moosader_team = "La ecipo de Moosader",
    credits_other = "Otra recursos",

    credits_name_racheljmorris = "Rachel Jane Morris",
    credits_role_racheljmorris = "Xef de programi, Xef de arte",

    credits_name_teacoba = "Tea Coba",
    credits_role_teacoba = "Desinia de niveles",

    credits_name_jessicacapehart = "Jessica Capehart",
    credits_role_jessicacapehart = "Mercatiste, medios",

    credits_name_rebekahhamilton = "Rebekah Hamilton",
    credits_role_rebekahhamilton = "Desinia de niveles",

    credits_name_enmanueltoribio = "Enmanuel Toribio",
    credits_role_enmanueltoribio = "Programor",

    credits_name_rosemorris = "Rose Morris",
    credits_role_rosemorris = "Medios",

    credits_sound_effects_a = "Efetos TEMPORA de sona",
    credits_sound_effects_b = "BFXR.NET",

    credits_music_a = "TEMPORA musica",
    credits_music_b = "Kevin MacLeod",
    credits_music_c = "Incompetech.com",
    credits_music_d = "Carnivale Intrigue",
    credits_music_e = "Mariachi Snooze",
    credits_music_f = "Motherlode",
    credits_music_g = "Thief In The Night",
    credits_music_h = "Upbeat Forever",

    credits_font_a = "Tipo de letera",
    credits_font_b = "Google Noto Sans, OFL",

    help_line1_mobile = "Toca Kit per fa ce el salta.",
    help_line2_mobile = "En el es en la aira, toca Kit per fa ce el salta denova.",
    help_line3_mobile = "En Kit es en la aira, toca Fin per fa ce Kit salta.",
    help_line4_mobile = "Si Kit senta sur Fin, toca Fin fa ce Kit salta.",
    help_line5_mobile = "",
    help_line6_mobile = "",
    help_line7_mobile = "Recolie la ornetas e evita la ostaculos.",
    help_line8_mobile = "Recolie tota la ornetas e evita tota la ostaculos per oteni 3 stelas en un nivel.",
    help_line9_mobile = "Recolie cores per oteni un colpa plu.",

    help_line1_pc = "Clica Kit por fa ce el salta.",
    help_line2_pc = "En el es en la aira, clica Kit per fa ce el salta denova.",
    help_line2b_pc = "",

    help_line3_pc = "En Kit es en la aira, clica Fin per fa ce Kit salta.",
    help_line4_pc = "Si Kit senta sur Fin, clica Fin fa ce Kit salta.",
    help_line4b_pc = "",
    help_line5_pc = "Tu pote ance usa la teclas dirijal BASA e ALTA",
    help_line6_pc = "per fa ce Fin e Kit salta.",
    help_line7_pc = "Recolie ornetas!",
    help_line8_pc = "Evita ostaculos!",
    help_line9_pc = "Recolie cores per oteni un colpa plu.",
    help_line10_pc = "Recolie tota la ornetas e evita tota la ostaculos per oteni 3 stelas en un nivel.",
    help_line10b_pc = "",

    cutscene_w1_c1a = "En plaia solosa e calda",
    cutscene_w1_c1b = "En dia solosa e calda",
    cutscene_w1_c1c = "La gateta Kit vade pexa.",

    cutscene_w1_c2a = "El no es destrosa",
    cutscene_w1_c2b = "car es ancora joven,",
    cutscene_w1_c2c = "ma el es persistente e no abandona.",

    cutscene_w1_c3a = "Pos oras de pratica...",
    cutscene_w1_c5a = "...Kit oteni se premio!",
    cutscene_w1_c6a = "(Con alga de aida!)",

    cutscene_w1_c7a = "Fin dise \"Alo!\"",
    cutscene_w1_c7b = "e Kit dise \"He!\"",

    cutscene_w1_c8a = "Un nova amia ia comensa...",
    cutscene_w1_c8b = "...construida sur se ama comun per pexes",

    cutscene_w1_c9a = "Fin ofre porta Kit per trova plu pexes.",
    cutscene_w1_c9b = "Kit monta pasionosa.",

    cutscene_w1_c10a = "Espeta nos, pexes !",
}
