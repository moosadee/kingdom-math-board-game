#include "MathKingdomQuestApp.hpp"

#include "states/TitleState.hpp"
//#include "states/LanguageSelectState.hpp"
//#include "states/LevelEditorState.hpp"
//#include "states/JuggleGameState.hpp"
//#include "states/CustomLevelLoaderState.hpp"
//#include "states/SplashScreenState.hpp"
#include "states/CharacterCustomizerState.hpp"

#include "kuko/utilities/Platform.hpp"

#include <iostream>

std::string MathKingdomQuestApp::m_gotoLevel = "";

MathKingdomQuestApp::MathKingdomQuestApp()
{
    Logger::Setup();
    Logger::Out( "Application Begin!" );
    m_contentPath = "content/";
    m_goToLanguageSelect = false;
    Setup();
}

MathKingdomQuestApp::~MathKingdomQuestApp()
{
    Logger::Out( "Application Destroy!" );
    Cleanup();
}

void MathKingdomQuestApp::Run()
{
    if ( m_goToLanguageSelect )
    {
        // Go to language select first
        kuko::LanguageManager::AddLanguage( "common", m_contentPath + "languages/common.lua" );
        m_stateManager.PushState( "language_select" );
    }
    else
    {
        // Go to title state first
        kuko::LanguageManager::AddLanguage( "primary", m_contentPath + "languages/" + kuko::ConfigManager::GetOption( "language" ) + ".lua" );
//        m_stateManager.PushState( "splash" );
        //m_stateManager.SwitchState( "cutscene" );
        m_stateManager.PushState( "title" );
    }

    Logger::Out( "Begin main game loop", "MathKingdomQuestApp::Run()" );
    kuko::Application::ResetStepTimer();
    while ( !m_stateManager.IsDone() )
    {
        // Timer
        kuko::Application::TimerStart();

        // Update
        m_stateManager.UpdateCurrentState();

        kuko::Application::ResetStepTimer();

        // Draw
        kuko::Application::BeginDraw();
        m_stateManager.DrawCurrentState();
        kuko::Application::EndDraw();

        // Timer
        kuko::Application::TimerUpdate();
    }
}

void MathKingdomQuestApp::Cleanup()
{
    Logger::Out( "Clean up the game!", "Cleanup" );
    kuko::ConfigManager::Cleanup();
    kuko::Application::End();
    Logger::Cleanup();
}

void MathKingdomQuestApp::Setup()
{
    KukoSetup();
    AppSetup();
}

void MathKingdomQuestApp::KukoSetup()
{
    Logger::Out( "Set up the game!", "MathKingdomQuestApp::Setup()" );

    // Lua Manager
    kuko::LuaManager::Setup();

    // Config Manager
    kuko::ConfigManager::Setup();
    std::vector<std::string> configSettings = {
        "language", "screen_width", "screen_height", "music_volume", "sound_volume",
        "vsync", "fullscreen", "borderless", "savegame_count",
        "savegame_1", "savegame_2", "savegame_3", "savegame_4", "savegame_5", "savegame_6", "savegame_7", "savegame_8"
    };

    OperatingSystem platform = Platform::GetOperatingSystem();
    Logger::Out( "I think that the operating system build is: " + Platform::GetOperatingSystemName( platform ), "MathKingdomQuestApp::KukoSetup" );

    bool firstRun = ( kuko::ConfigManager::LoadConfig( configSettings ) == false );
    if ( firstRun )
    {
        Logger::Out( "This is a first run - Set up the initial config file.", "Setup" );
        kuko::ConfigManager::SetOption( "language", "english" );
        kuko::ConfigManager::SetOption( "screen_width", "1280" );
        kuko::ConfigManager::SetOption( "screen_height", "720" );
        kuko::ConfigManager::SetOption( "music_volume", "100" );
        kuko::ConfigManager::SetOption( "sound_volume", "100" );
        kuko::ConfigManager::SetOption( "fullscreen", "false" );
        kuko::ConfigManager::SetOption( "borderless", "false" );

        if ( platform == WINDOWS32 || platform == WINDOWS64 )
        {
            kuko::ConfigManager::SetOption( "vsync", "false" );
        }
        else
        {
            kuko::ConfigManager::SetOption( "vsync", "true" );
        }
        kuko::ConfigManager::SaveConfig();
        // Need to go to language select if this is the first run.
        m_goToLanguageSelect = true;
    }

    // Language Manager
    kuko::LanguageManager::Setup();
    kuko::LanguageManager::AddLanguage( "common", m_contentPath + "languages/common.lua" );

    bool useVsync = ( kuko::ConfigManager::GetOption( "vsync" ) == "true" );
    bool fullscreen = false;
        //( kuko::ConfigManager::GetOption( "fullscreen" ) == "true" );
    bool borderless = true;
        //( kuko::ConfigManager::GetOption( "borderless" ) == "true" );
    bool fitToScreen = false;
    // Application
    kuko::Application::Start( kuko::LanguageManager::Text( "game_title" ) + " - " + kuko::LanguageManager::Text( "game_creator" ),
        StringUtil::StringToInt( kuko::ConfigManager::GetOption( "screen_width" ) ),
        StringUtil::StringToInt( kuko::ConfigManager::GetOption( "screen_height" ) ),
        1280, 720, useVsync, fullscreen, borderless, fitToScreen );

    // Font Manager
    kuko::FontManager::Setup();
    kuko::FontManager::AddFont( "main", m_contentPath + "fonts/NotoSans-Bold.ttf", 60 );

    // Input Manager
    kuko::InputManager::Setup();

    // Sound Manager
//    kuko::SoundManager::AddMusic( "title", m_contentPath + "audio/UpbeatForever_Incompetech.mp3" );

    kuko::SoundManager::AddSound( "button_confirm", m_contentPath + "audio/confirm.wav" );
    kuko::SoundManager::AddSound( "button_cancel", m_contentPath + "audio/cancel.wav" );

    kuko::SoundManager::SetMusicVolume( StringUtil::StringToInt( kuko::ConfigManager::GetOption( "music_volume" ) ) );
    kuko::SoundManager::SetSoundVolume( StringUtil::StringToInt( kuko::ConfigManager::GetOption( "sound_volume" ) ) );
}

void MathKingdomQuestApp::AppSetup()
{
    // Add states
//    m_stateManager.AddState( "splash",             new SplashScreenState( m_contentPath ) );
    m_stateManager.AddState( "title",                   new TitleState( m_contentPath ) );
    m_stateManager.AddState( "character_customizer",    new CharacterCustomizerState( m_contentPath ) );
}

std::string MathKingdomQuestApp::GetGotoLevel()
{
    return m_gotoLevel;
}

void MathKingdomQuestApp::SetGotoLevel( const std::string& level )
{
    m_gotoLevel = level;
}







