NG_DEBUG:SETUP()
NG_DEBUG:LOG( "main.lua", "info", "Program begins!" )

NG_CONFIG_MANAGER:SETUP()
NG_ASSET_MANAGER:SETUP()
NG_STATE_MANAGER:SETUP()
NG_RENDERER:SETUP()

NG_STATE_MANAGER:ADD_STATE( "CHARACTER_SELECT", CharacterSelectState.new( { name = "CHARACTER_SELECT" } ) )
NG_STATE_MANAGER:ADD_STATE( "BOARDGAME", BoardgameState.new( { name = "BOARDGAME" } ) )
NG_STATE_MANAGER:ADD_STATE( "END_STATE", EndState.new( { name = "END_STATE" } ) )

NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg", path = "content/graphics/ui/default-background.png" } )
frameChecker = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg" ) )
stage:addChild( frameChecker )

-- DEBUGGING INFO

local playerOptions = {
	{ name = "Ruby", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 0, avatar = 1, questionsAnswered = 1, totalCorrect = 0 },
	{ name = "Rose", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 180, avatar = 2, questionsAnswered = 1, totalCorrect = 0 },
	{ name = "Natalie", enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 90, avatar = 3, questionsAnswered = 1, totalCorrect = 0 },
	{ name = "Basel", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 270, avatar = 4, questionsAnswered = 1, totalCorrect = 0 }
}

NG_CONFIG_MANAGER:SET( "playerOptions", playerOptions )

--NG_STATE_MANAGER:GOTO_STATE( "BOARDGAME" )
NG_STATE_MANAGER:GOTO_STATE( "CHARACTER_SELECT" )
--NG_STATE_MANAGER:GOTO_STATE( "END_STATE" )

function CheckForStateChange()
	local gotoState = NG_STATE_MANAGER:GET_CURRENT_STATE():GotoState()
	if ( gotoState ~= "" ) then
		NG_DEBUG:LOG( "main.lua CheckForStateChange()", "info", "Go to state \"" .. gotoState .. "\"" )
		NG_STATE_MANAGER:GOTO_STATE( gotoState )
	end
end

frameChecker:addEventListener( Event.ENTER_FRAME, CheckForStateChange, self )

--NG_DEBUG:TEARDOWN()