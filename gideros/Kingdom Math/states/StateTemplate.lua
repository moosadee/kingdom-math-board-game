BoardgameState = Core.class()

function BoardgameState:RotateView( angle )
	NG_DEBUG:LOG( "BoardgameState:RotateView", "info", "Angle: " .. angle )
	
		self.viewport:setAnchorPosition( 720/2, 1280/2 )
		self.viewport:setPosition( 720/2, 1280/2 )
	
	if ( angle == 90 ) then
		self.viewport:setRotation( angle )
	else
		self.viewport:setRotation( angle )
	end
		
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

-- Functions that all states should have

function BoardgameState:init( options )
	NG_DEBUG:LOG( "BoardgameState:init", "info", "State key \"" .. options.name .. "\"" )
	self.buttons = {}
	self.labels = {}
	self.images = {}
	self.name = options.name
	self.view = ""
	self.screenTopY = (1280 - 720) / 2
	self.screenWidthHeight = 720
	self.viewAngle = 0
	self.viewport = Viewport.new()
	
	self.gotoState = ""
end

function BoardgameState:GotoState()
	return self.gotoState
end

function BoardgameState:Start()
	self:Setup()
end

function BoardgameState:Stop()
	self.Teardown()
end

function BoardgameState:Setup()
	NG_DEBUG:LOG( "BoardgameState:Setup", "info", "Function begin" )
	
	self.view = "player-spin"
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg", path = "content/graphics/ui/default-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg-sub", path = "content/graphics/ui/default-background-sub.png" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "main", size = 60, path = "content/fonts/Sniglet-Regular.ttf" } )

	self.background = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg" ) )
	self.subBackground = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg-sub" ) )
	self.subBackground:setPosition( 0, (1280-720)/2 )
	
	self.screenTopY = (1280 - 720) / 2
	
	self.labels["debug"] = ngLabel.new( { drawSurface = self.viewport, x = 100, y = (1280/2), text = "BOARDGAME STATE", font = NG_ASSET_MANAGER:GET_FONT( "main" ), color = 0xffe177 } )
	
	self.labels["header"] = ngLabel.new( { drawSurface = self.viewport, x = 150, y = self.screenTopY + 50, text = "How many players?", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0xffffff } )
	
	self.viewport:addChild( self.subBackground )
	
	for key, label in pairs( self.labels ) do
		label:Draw()
	end
	
	stage:addChild( self.background )
	stage:addChild( self.viewport )
	
	self.gotoState = ""
	
	-- callbacks
	stage:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
end

function BoardgameState:Teardown()
	NG_DEBUG:LOG( "BoardgameState:Teardown", "info", "Function begin" )
end

function BoardgameState:Update()
end

function BoardgameState:Event_AndroidKey( event )
	NG_DEBUG:LOG( "BoardgameState:Event_AndroidKey", "info", "Function begin" )
end

function BoardgameState:Event_MouseClick( event )
	NG_DEBUG:LOG( "BoardgameState:Event_MouseClick", "info", "Function begin" )
end

function BoardgameState:Event_FrameUpdate()
end
