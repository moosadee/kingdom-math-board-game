EndState = Core.class()

function EndState:RotateView( angle )
	NG_DEBUG:LOG( "EndState:RotateView", "info", "Angle: " .. angle )
	
		self.viewport:setAnchorPosition( 720/2, 1280/2 )
		self.viewport:setPosition( 720/2, 1280/2 )
	
	if ( angle == 90 ) then
		self.viewport:setRotation( angle )
	else
		self.viewport:setRotation( angle )
	end
		
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

-- Functions that all states should have

function EndState:init( options )
	NG_DEBUG:LOG( "EndState:init", "info", "State key \"" .. options.name .. "\"" )
	self.buttons = {}
	self.labels = {}
	self.images = {}
	self.name = options.name
	self.view = ""
	self.screenTopY = (1280 - 720) / 2
	self.screenWidthHeight = 720
	self.viewAngle = 0
	self.viewport = Viewport.new()
	
	self.gotoState = ""
end

function EndState:GotoState()
	return self.gotoState
end

function EndState:Start()
	self:Setup()
end

function EndState:Stop()
	self.Teardown()
end

function EndState:Setup()
	NG_DEBUG:LOG( "EndState:Setup", "info", "Function begin" )
	
	self.view = "player-spin"
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg", path = "content/graphics/ui/default-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg-sub", path = "content/graphics/ui/default-background-sub.png" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "main", size = 60, path = "content/fonts/Sniglet-Regular.ttf" } )

	self.background = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg" ) )
	self.subBackground = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg-sub" ) )
	self.subBackground:setPosition( 0, (1280-720)/2 )
	
	self.screenTopY = (1280 - 720) / 2
	self.gotoState = ""
	
	NG_RENDERER:SETUP()
	
	self.playerOptions = NG_CONFIG_MANAGER:GET( "playerOptions" )
	local p1Avatar = self.playerOptions[1][ "avatar" ]
	local p2Avatar = self.playerOptions[2][ "avatar" ]
	local p3Avatar = self.playerOptions[3][ "avatar" ]
	local p4Avatar = self.playerOptions[4][ "avatar" ]
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-1", path = "content/graphics/avatars/avatar-" .. p1Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-2", path = "content/graphics/avatars/avatar-" .. p2Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-3", path = "content/graphics/avatars/avatar-" .. p3Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-4", path = "content/graphics/avatars/avatar-" .. p4Avatar .. ".png" } )

	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-bg", path = "content/graphics/ui/button-med.png" } )
	
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "main", size = 60, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "sub", size = 30, path = "content/fonts/Sniglet-Regular.ttf" } )
	
	
	NG_RENDERER:ADD_DRAWABLES( "status",  ngLabel.new( { 
		drawSurface = NG_RENDERER:LAYER( 2 ), x = 250, y = self.screenTopY + 100, color = 0xffff00, 
		font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = "You Win!" 
	} ) )
	
	local tx, ty = 0, 400
	local spacing = 720 / 4
	local scale = 1
	local stagger = 200
	NG_RENDERER:ADD_DRAWABLES( "avatar-1", ngImage.new( { 
		drawSurface = NG_RENDERER:LAYER( 2 ), x = spacing * 0, y = ty, 
		background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-1" ), scaleX = scale, scaleY = scale } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "avatar-2", ngImage.new( { 
		drawSurface = NG_RENDERER:LAYER( 2 ), x = spacing * 1, y = ty+stagger,
		background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-2" ), scaleX = scale, scaleY = scale } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "avatar-3", ngImage.new( { 
		drawSurface = NG_RENDERER:LAYER( 3 ), x = spacing * 2, y = ty, 
		background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-3" ), scaleX = scale, scaleY = scale } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "avatar-4", ngImage.new( {
		drawSurface = NG_RENDERER:LAYER( 3 ), x = spacing * 3, y = ty+stagger, 
		background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-4" ), scaleX = scale, scaleY = scale } )	) 
	
	NG_RENDERER:ADD_DRAWABLES( "name1",  ngLabel.new( { text = self.playerOptions[1]["name"], x = spacing * 0 + 10, y = ty + 200, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "name2",  ngLabel.new( { text = self.playerOptions[2]["name"], x = spacing * 1 + 10, y = ty + 200+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "name3",  ngLabel.new( { text = self.playerOptions[3]["name"], x = spacing * 2 + 10, y = ty + 200, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "name4",  ngLabel.new( { text = self.playerOptions[4]["name"], x = spacing * 3 + 10, y = ty + 200+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	
	NG_RENDERER:ADD_DRAWABLES( "q1",  ngLabel.new( { text = self.playerOptions[1]["totalCorrect"] .. " Questions", x = spacing * 0 + 10, y = ty + 230, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "q2",  ngLabel.new( { text = self.playerOptions[2]["totalCorrect"] .. " Questions", x = spacing * 1 + 10, y = ty + 230+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "q3",  ngLabel.new( { text = self.playerOptions[3]["totalCorrect"] .. " Questions", x = spacing * 2 + 10, y = ty + 230, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "q4",  ngLabel.new( { text = self.playerOptions[4]["totalCorrect"] .. " Questions", x = spacing * 3 + 10, y = ty + 230+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	local percent = math.floor( self.playerOptions[1]["totalCorrect"] / self.playerOptions[1]["questionsAnswered"] * 100 )
	NG_RENDERER:ADD_DRAWABLES( "c1",  ngLabel.new( { text = percent .. "% Correct", x = spacing * 0 + 10, y = ty + 260, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	local percent = math.floor( self.playerOptions[2]["totalCorrect"] / self.playerOptions[2]["questionsAnswered"] * 100 )
	NG_RENDERER:ADD_DRAWABLES( "c2",  ngLabel.new( { text = percent .. "% Correct", x = spacing * 1 + 10, y = ty + 260+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	local percent = math.floor( self.playerOptions[3]["totalCorrect"] / self.playerOptions[3]["questionsAnswered"] * 100 )
	NG_RENDERER:ADD_DRAWABLES( "c3",  ngLabel.new( { text = percent .. "% Correct", x = spacing * 2 + 10, y = ty + 260, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	local percent = math.floor( self.playerOptions[4]["totalCorrect"] / self.playerOptions[4]["questionsAnswered"] * 100 )
	NG_RENDERER:ADD_DRAWABLES( "c4",  ngLabel.new( { text = percent .. "% Correct", x = spacing * 3 + 10, y = ty + 260+stagger, drawSurface = NG_RENDERER:LAYER( 2 ), color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "sub" ) } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "go-back",  ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), 
		x = 280, y = 900, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = "< Back", textColor = 0x000000, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), 
		textX = 20, textY = 60 } ) )
	
	
	NG_RENDERER:DRAW()
	
	-- callbacks
	stage:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
end

function EndState:Teardown()
	NG_DEBUG:LOG( "EndState:Teardown", "info", "Function begin" )
end

function EndState:Update()
end

function EndState:Event_AndroidKey( event )
	NG_DEBUG:LOG( "EndState:Event_AndroidKey", "info", "Function begin" )
end

function EndState:Event_MouseClick( event )
	NG_DEBUG:LOG( "EndState:Event_MouseClick", "info", "Function begin" )
	
	if ( NG_RENDERER:GET_DRAWABLE( "go-back" ):IsClicked( event ) ) then
		self.gotoState = "CHARACTER_SELECT"
	end
end

function EndState:Event_FrameUpdate()
end
