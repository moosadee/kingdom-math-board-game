BoardgameState = Core.class()

function BoardgameState:CreateBoard()
	NG_DEBUG:LOG( "BoardgameState:CreateBoard", "info", "Function begin" )
	self.boardPath = { 
		{ tilex = 4, tiley = 1, start = true, neighbor_left = 2 }, -- index = 1
		{ tilex = 3, tiley = 1, neighbor_left = 3, neighbor_right = 1 },
		{ tilex = 2, tiley = 1, enemy = "mosquito", health = 3, neighbor_down = 4, neighbor_right = 2 },
		{ tilex = 2, tiley = 2, neighbor_up = 3, neighbor_down = 5 },
		{ tilex = 2, tiley = 3, enemy = "mosquito", health = 3, neighbor_up = 4, neighbor_down = 6 },
		{ tilex = 2, tiley = 4, neighbor_up = 5, neighbor_down = 7 },
		{ tilex = 2, tiley = 5, neighbor_left = 8, neighbor_right = 9, neighbor_up = 6 },
		{ tilex = 1, tiley = 5, enemy = "mosquito", health = 3, neighbor_right = 7, neighbor_down = 11 },
		{ tilex = 3, tiley = 5, enemy = "mosquito", health = 3, neighbor_left = 7, neighbor_right = 10 },
		{ tilex = 4, tiley = 5, neighbor_left = 9, neighbor_down = 12 },
		{ tilex = 1, tiley = 6, enemy = "mosquito", health = 3, neighbor_up = 8, neighbor_down = 13 },
		{ tilex = 4, tiley = 6, enemy = "mosquito", health = 3, neighbor_up = 10, neighbor_down = 16 },
		{ tilex = 1, tiley = 7, neighbor_up = 11, neighbor_right = 14 },
		{ tilex = 2, tiley = 7, neighbor_left = 13, neighbor_right = 15 },
		{ tilex = 3, tiley = 7, enemy = "mosquito", health = 3, neighbor_left = 14, neighbor_right = 16, neighbor_down = 17 },
		{ tilex = 4, tiley = 7, neighbor_left = 15, neighbor_up = 12 },
		{ tilex = 3, tiley = 8, enemy = "mosquito-king", health = 5, neighbor_up = 15, boss = true },
	}
end

function BoardgameState:DebugInfo()
	NG_DEBUG:LOG( "BoardgameState:DebugInfo", "info", "PLAYER POSITIONS" )
	
	if ( self.playerPositions ~= nil ) then
		for i = 1, 4 do
			NG_DEBUG:LOG( "BoardgameState:DebugInfo", "info", "Player " .. i .. " position: x = " .. self.playerPositions[i]["x"] .. ", y = " .. self.playerPositions[i]["y"] )
		end
	end
	
	NG_DEBUG:LOG( "BoardgameState:DebugInfo", "info", "BOARD STATE" )
	for key, tile in pairs( self.boardPath ) do
		local str = ""
		str = str .. "\t TILE " .. key .. "\t tilex: " .. tile["tilex"] .. ", tiley: " .. tile["tiley"]
		if ( tile["enemy"] ~= nil ) then
			str = str .. "\t enemy: " .. tile["enemy"]
		end
		if ( tile["health"] ~= nil ) then
			str = str .. "\t health: " .. tile["health"]
		end
		NG_DEBUG:LOG( "BoardgameState:DebugInfo", "info", str )
	end
end

function BoardgameState:InitializeTurn( whoseTurn )
	NG_DEBUG:LOG( "BoardgameState:InitializeTurn", "info", "Function begin, turn = " .. whoseTurn )
	self.currentTurn = whoseTurn
	self.view = "player-move"
	
	if ( self.currentTurn > self.maxPlayers ) then
		self.currentTurn = 1
	end
	
	local playerName = self.playerOptions[ self.currentTurn ][ "name" ]
	
	NG_RENDERER:GET_DRAWABLE("message1"):SetText( "Player " .. self.currentTurn )
	NG_RENDERER:GET_DRAWABLE("message2"):SetText( playerName )

	NG_RENDERER:DRAW()
	
	self:HighlightMovableTiles()
end

function BoardgameState:CheckPlayerInput_Move( event )
	local nextState = ""
	
	for key, tile in pairs( self.highlights ) do
		if ( tile.button:IsClicked( event ) ) then
			-- Move this player to that position
			local px, py = self:GetAdjustedCoordinates( tile["tilex"], tile["tiley"], self.tileWidthHeight )
			
			self.playerPositions[ self.currentTurn ]["x"] = tile["tilex"]
			self.playerPositions[ self.currentTurn ]["y"] = tile["tiley"]
			NG_RENDERER:GET_DRAWABLE("avatar-" .. self.currentTurn):SetPosition( 
				px + self.playerOffsets[self.currentTurn]["x"], 
				py + self.playerOffsets[self.currentTurn]["y"] )
			self.currentTile = key
			
			if ( tile["enemy"] ~= nil ) then
				nextState = "fight-enemy"
			else
				nextState = "next-player"
			end
		end
	end
	
	if ( nextState == "fight-enemy" ) then
		-- Begin battle
		self:Clear_PlayerMove()
		self:InitializeBattle()
	elseif ( nextState == "next-player" ) then
		self:InitializeTurn( self.currentTurn + 1 )
	end
end

function BoardgameState:Clear_PlayerMove()
	NG_RENDERER:GET_DRAWABLE("message1"):SetText( "" )
	NG_RENDERER:GET_DRAWABLE("message2"):SetText( "" )
end

function BoardgameState:Swap( a, b )
	c = a
	a = b
	b = a
	return a, b
end

function BoardgameState:GenerateMathProblem()
	NG_DEBUG:LOG( "BoardgameState:GenerateMathProblem", "info", "Function begin" )
	self.questionNum1 = math.random( 1, 12 )
	self.questionNum2 = math.random( 1, 12 )
	
	local mathOperation = 0
	while ( mathOperation == 0 ) do
		mathOperation = math.random( 1,4 )		-- 1 = add, 2 = subtract, 3 = multiply, 4 = divide
		
		-- TODO: Make sure players can't de-select every type of math operation
		if ( 
			( mathOperation == 1 and self.playerOptions[ self.currentTurn ][ "enableAddition" ] == false ) or
			( mathOperation == 2 and self.playerOptions[ self.currentTurn ][ "enableSubtraction" ] == false ) or
			( mathOperation == 3 and self.playerOptions[ self.currentTurn ][ "enableMultiplication" ] == false ) or
			( mathOperation == 4 and self.playerOptions[ self.currentTurn ][ "enableDivision" ] == false )
			) then
			mathOperation = 0
		end
	end
	
	if ( mathOperation == 1 ) then
		self.questionOp = "+"
		self.answer = self.questionNum1 + self.questionNum2
		
	elseif ( mathOperation == 2 ) then
		self.questionOp = "-"
		
		-- Make sure result is positive:
		if ( self.questionNum1 < self.questionNum2 ) then
			self.questionNum1, self.questionNum2 = self:Swap( self.questionNum1, self.questionNum2 )
		end
		
		self.answer = self.questionNum1 - self.questionNum2
		
	elseif ( mathOperation == 3 ) then
		self.questionOp = "x"
		self.answer = self.questionNum1 * self.questionNum2
		
	elseif ( mathOperation == 4 ) then
		self.questionOp = "÷"	
		
		-- Make first number biggest:
		if ( self.questionNum1 < self.questionNum2 ) then
			self.questionNum1, self.questionNum2 = self:Swap( self.questionNum1, self.questionNum2 )
		end
		
		-- Make sure result is an integer:
		while ( math.fmod( self.questionNum1, self.questionNum2 ) ~= 0 ) do
			self.questionNum1 = self.questionNum1 + 1
		end
		
		self.answer = self.questionNum1 / self.questionNum2
	end
	
	NG_RENDERER:GET_DRAWABLE( "math-problem" ):SetText( self.questionNum1 .. " " .. self.questionOp .. " " .. self.questionNum2 .. " = " ) 
	NG_RENDERER:DRAW()
	
	NG_DEBUG:LOG( "BoardgameState:GenerateMathProblem", "info", "Math question: " .. self.questionNum1 .. self.questionOp .. self.questionNum2 )
end

function BoardgameState:CheckMathAnswer()
	local playerAnswer = self.hundreds * 100 + self.tens * 10 + self.ones * 1
	
	NG_DEBUG:LOG( "BoardgameState:CheckMathAnswer", "info", "Correct answer: " .. self.answer .. ", player answer: " .. playerAnswer )
	
	if ( self.answer == playerAnswer ) then		
		local removeHeart = self.boardPath[ self.currentTile ][ "health" ]
		self.boardPath[ self.currentTile ][ "health" ] = self.boardPath[ self.currentTile ][ "health" ] - 1
		
		if ( NG_RENDERER:GET_DRAWABLE( "heart-" .. removeHeart ) ~= nil ) then
			NG_RENDERER:GET_DRAWABLE( "heart-" .. removeHeart ):SetPosition( -100, -100 )
		end
		NG_DEBUG:LOG( "BoardgameState:CheckMathAnswer", "info", "Enemy health: " .. self.boardPath[ self.currentTile ][ "health" ] )
		
		if ( self.boardPath[ self.currentTile ][ "health" ] == 0 ) then
			-- Enemy is dead
			NG_ASSET_MANAGER:PLAY_SOUND( "enemy-dead" )
			self:AnimatedStatus( "Great job!" )
			self:HoldForEvent( "enemy dead" )
			NG_RENDERER:GET_DRAWABLE( "enemy-battle" ):SetRotation( { angle = 180 } )
			NG_RENDERER:GET_DRAWABLE( "enemy-battle" ):SetPosition( 550, 550 )
		else
			NG_ASSET_MANAGER:PLAY_SOUND( "right-answer" )
			self:GenerateMathProblem()
			self:AnimatedStatus( "Correct!" )
			self.playerOptions[self.currentTurn]["totalCorrect"] = self.playerOptions[self.currentTurn]["totalCorrect"] + 1
		end
	else
		-- Show "Wrong" message
		NG_ASSET_MANAGER:PLAY_SOUND( "wrong-answer" )
		self:AnimatedStatus( "Wrong!" )
		self:HoldForEvent( "end battle" )
	end
			
	self.playerOptions[self.currentTurn]["questionsAnswered"] = self.playerOptions[self.currentTurn]["questionsAnswered"] + 1
end

function BoardgameState:HoldForEvent( eventName )
	self.waitingEvent = eventName
end

function BoardgameState:CheckForWaitingEvent()
	if ( self.animationTimer <= 0 and self.waitingEvent ~= "" ) then
		if ( self.waitingEvent == "end battle" ) then
			self:BattleEnd()		
		elseif ( self.waitingEvent == "enemy dead" ) then
			self:EnemyDead()
			self:BattleEnd()
		end
		
		self:ClearHold()
	end
end

function BoardgameState:ClearHold()
	self.waitingEvent = ""
end

function BoardgameState:AnimatedStatus( text )
	if ( NG_RENDERER:GET_DRAWABLE( "status" ) ~= nil ) then
		NG_RENDERER:GET_DRAWABLE( "status" ):SetText( text ) 
		self.animationTimer = 100
	end
end

function BoardgameState:ClearAnimatedStatus()
	if ( NG_RENDERER:GET_DRAWABLE( "status" ) ~= nil ) then
		NG_RENDERER:GET_DRAWABLE( "status" ):SetText( "" ) 
	end
end

function BoardgameState:EnemyDead()
	if ( self.boardPath[ self.currentTile ][ "boss" ] == true ) then
		self:KilledBoss()
	end
	
	self.boardPath[ self.currentTile ][ "enemy" ] = nil
	
	NG_DEBUG:LOG( "BoardgameState:EnemyDead", "info", "ENEMY DEAD - UPDATED BOARD" )
	self:DebugInfo()
end

function BoardgameState:KilledBoss()
	NG_DEBUG:LOG( "BoardgameState:KilledBoss", "info", "Function begin" )
	NG_CONFIG_MANAGER:SET( "playerOptions", self.playerOptions )
	self.gotoState = "END_STATE"
end

function BoardgameState:BattleEnd()
	self:InitializeBoardState()
	self:InitializeTurn( self.currentTurn + 1 )
	NG_RENDERER:DRAW()
end

function BoardgameState:InitializeBoardState()
	NG_DEBUG:LOG( "BoardgameState:InitializeBoardState", "info", "Function begin" )
	self.view = "player-move"
	NG_RENDERER:CLEAR_DRAWABLES()
	
	local tx, ty = 0, 0
	local scale = 0.7
	
	NG_RENDERER:ADD_DRAWABLES( "message1", ngLabel.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = 400, y = 275, text = "Player " .. self.currentTurn, font = NG_ASSET_MANAGER:GET_FONT( "main" ), color = 0xffff00 } ) )
	NG_RENDERER:ADD_DRAWABLES( "message2", ngLabel.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = 400, y = 375, text = playerName, font = NG_ASSET_MANAGER:GET_FONT( "big" ), color = 0xffff00 } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "board", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 1 ), x = -120, y = 0, background = NG_ASSET_MANAGER:GET_TEXTURE( "board" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "overlay", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 1 ), x = -120, y = 0, background = NG_ASSET_MANAGER:GET_TEXTURE( "overlay" ) } ) )
	
	tx, ty = self:GetAdjustedCoordinates( self.playerPositions[1]["x"], self.playerPositions[1]["y"], self.tileWidthHeight )
	NG_RENDERER:ADD_DRAWABLES( "avatar-1", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = tx + self.playerOffsets[1]["x"], y = ty + self.playerOffsets[1]["y"], background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-1" ), scaleX = scale, scaleY = scale } ) )
	
	tx, ty = self:GetAdjustedCoordinates( self.playerPositions[2]["x"], self.playerPositions[2]["y"], self.tileWidthHeight )
	NG_RENDERER:ADD_DRAWABLES( "avatar-2", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = tx + self.playerOffsets[2]["x"], y = ty + self.playerOffsets[2]["y"], background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-2" ), scaleX = scale, scaleY = scale } ) )
	
	tx, ty = self:GetAdjustedCoordinates( self.playerPositions[3]["x"], self.playerPositions[3]["y"], self.tileWidthHeight )
	NG_RENDERER:ADD_DRAWABLES( "avatar-3", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = tx + self.playerOffsets[3]["x"], y = ty + self.playerOffsets[3]["y"], background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-3" ), scaleX = scale, scaleY = scale } ) )
	
	tx, ty = self:GetAdjustedCoordinates( self.playerPositions[4]["x"], self.playerPositions[4]["y"], self.tileWidthHeight )
	NG_RENDERER:ADD_DRAWABLES( "avatar-4", ngImage.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = tx + self.playerOffsets[4]["x"], y = ty + self.playerOffsets[4]["y"], background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-4" ), scaleX = scale, scaleY = scale } )	) 
	
	self:HighlightMovableTiles()
	
	NG_DEBUG:LOG( "BoardgameState:InitializeBoardState", "info", "INITIALIZE BOARD" )
	self:DebugInfo()
	
	self.enemies = {}
	for key, tile in pairs( self.boardPath ) do
		if ( tile["enemy"] ~= nil and tile[ "health" ] ~= 0 ) then
			tx, ty = self:GetAdjustedCoordinates( tile["tilex"], tile["tiley"], self.tileWidthHeight )
			local enemyImage = ngImage.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = tx, y = ty, background = NG_ASSET_MANAGER:GET_TEXTURE( tile["enemy"] ) } )
			NG_RENDERER:ADD_DRAWABLES( "enemy-" .. #self.enemies, enemyImage )
			
			hp = tile["health"]
			tile["image"] = enemyImage
			table.insert( self.enemies, { type = tile["enemy"], health = hp, image = enemyImage } )  -- Don't need this
			
			-- Add health
			spacing = self.tileWidthHeight / hp
			for i=0, hp - 1 do
				local assetName = "enemy_x-" .. tile["tilex"] .. "_y-" .. tile["tiley"] .. "_hp-" .. i
				local heartX = tx + ( i * spacing )
				local heartY = ty
				NG_RENDERER:ADD_DRAWABLES( assetName, ngImage.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = heartX, y = heartY, background = NG_ASSET_MANAGER:GET_TEXTURE( "heart" ) } ) )
			end
		end
	end
	
	stage:addChild( self.background )
end

function BoardgameState:InitializeBattle()
	NG_DEBUG:LOG( "BoardgameState:InitializeBattle", "info", "Function begin" )
	self.view = "player-battle"
	
	NG_RENDERER:CLEAR_DRAWABLES()
	
	NG_DEBUG:LOG( "BoardgameState:InitializeBattle", "info", "INITIALIZE BATTLE" )
	self:DebugInfo()
	
	local enemyInfo = self.boardPath[ self.currentTile ]
	local tile = self.boardPath[ self.currentTile ]
	
	local scrollerTop = 400 + self.screenTopY
	local numberView = scrollerTop + 75
	local scrollerDown = numberView + 147
	
	self.ones = 0
	self.tens = 0
	self.hundreds = 0
	self.questionNum1 = 0
	self.questionOp = "+"
	self.questionNum2 = 0
	self.animationTimer = 0
	
	self:CreateBattleDrawables()
	self:GenerateMathProblem()
	
	-- Enemy health
	local spacing = self.tileWidthHeight / enemyInfo["health"]
	for i=0, enemyInfo["health"] - 1 do
		local assetName = "heart-" .. (i+1)
		local heartX = 500 + ( i * spacing )
		local heartY = 425
		NG_RENDERER:ADD_DRAWABLES( assetName, ngImage.new( { drawSurface = NG_RENDERER:LAYER( 5 ), x = heartX, y = heartY, background = NG_ASSET_MANAGER:GET_TEXTURE( "heart" ) } ) )
	end
	
	NG_RENDERER:DRAW()
end

function BoardgameState:AdjustNumber( number, amount )
	number = number + amount
	if ( number < 0 ) then
		number = 9
	elseif ( number > 9 ) then
		number = 0
	end
	
	return number
end

function BoardgameState:CheckPlayerInput_Battle( event )
	if ( NG_RENDERER:GET_DRAWABLE( "submit-answer" ):IsClicked( event ) ) then
		self:CheckMathAnswer()

	elseif ( NG_RENDERER:GET_DRAWABLE( "hundreds-down" ):IsClicked( event ) ) then
		self.hundreds = self:AdjustNumber( self.hundreds, -1 )
		NG_RENDERER:GET_DRAWABLE( "digit-hundreds" ):ChangeText( { text = self.hundreds } )
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		
	elseif ( NG_RENDERER:GET_DRAWABLE( "tens-down" ):IsClicked( event ) ) then
		self.tens = self:AdjustNumber( self.tens, -1 )
		NG_RENDERER:GET_DRAWABLE( "digit-tens" ):ChangeText( { text = self.tens } )
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		
	elseif ( NG_RENDERER:GET_DRAWABLE( "ones-down" ):IsClicked( event ) ) then
		self.ones = self:AdjustNumber( self.ones, -1 )
		NG_RENDERER:GET_DRAWABLE( "digit-ones" ):ChangeText( { text = self.ones } ) 
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		
	elseif ( NG_RENDERER:GET_DRAWABLE( "hundreds-up" ):IsClicked( event ) ) then
		self.hundreds = self:AdjustNumber( self.hundreds, 1 )
		NG_RENDERER:GET_DRAWABLE( "digit-hundreds" ):ChangeText( { text = self.hundreds } )
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		
	elseif ( NG_RENDERER:GET_DRAWABLE( "tens-up" ):IsClicked( event ) ) then
		self.tens = self:AdjustNumber( self.tens, 1 )
		NG_RENDERER:GET_DRAWABLE( "digit-tens" ):ChangeText( { text = self.tens } )
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		
	elseif ( NG_RENDERER:GET_DRAWABLE( "ones-up" ):IsClicked( event ) ) then
		self.ones = self:AdjustNumber( self.ones, 1 )
		NG_RENDERER:GET_DRAWABLE( "digit-ones" ):ChangeText( { text = self.ones } )
		NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
	end
	
	NG_RENDERER:DRAW()
end

function BoardgameState:GetAdjustedCoordinates( tileX, tileY, tileWidthHeight )
	local x = (tileX-1) * tileWidthHeight + 40		-- Adjust for the half-tiles on left/right, and lua starting count at 1.
	local y = (tileY-1) * tileWidthHeight
	return x, y
end

-- Functions that all states should have

function BoardgameState:init( options )
	NG_DEBUG:LOG( "BoardgameState:init", "info", "State key \"" .. options.name .. "\"" )
	self.buttons = {}
	self.labels = {}
	self.images = {}
	self.avatars = {}
	self.board = {}
	self.highlights = {}
	self.name = options.name
	self.view = ""
	self.maxPlayers = 0
	self.screenTopY = (1280 - 720) / 2
	self.screenWidthHeight = 720
	self.viewAngle = 0
	self.viewport = Viewport.new()
	self.waitingEvent = ""
	self.animationTimer = 0
	
	self.gotoState = ""
end

function BoardgameState:GotoState()
	return self.gotoState
end

function BoardgameState:Start()
	self:Setup()
end

function BoardgameState:Stop()
	self.Teardown()
end

function BoardgameState:Setup()
	NG_DEBUG:LOG( "BoardgameState:Setup", "info", "Function begin" )
	self.playerOptions = NG_CONFIG_MANAGER:GET( "playerOptions" )
	self:RegisterAssets()
	
	self.view = "player-move"
	self.currentTurn = 1
	self.gotoState = ""	
	self.maxPlayers = 4	

	self.background = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg" ) )
	
	self.screenTopY = (1280 - 720 ) / 2
	self.tileWidthHeight = 160
	
	self.startPositionX = 4
	self.startPositionY = 1
	
	self.playerOffsets = { { x = 10, y = -5 }, { x = 70, y = -5 }, { x = -20, y = 45 }, { x = 40, y = 45 } }	
	
	self.playerPositions = { 
		{ x = self.startPositionX, y = self.startPositionY },
		{ x = self.startPositionX, y = self.startPositionY },
		{ x = self.startPositionX, y = self.startPositionY },
		{ x = self.startPositionX, y = self.startPositionY }
	}

	self:CreateBoard()
	self:InitializeBoardState()
	
	self:InitializeTurn( 1 )
	
	NG_RENDERER:DRAW()
	
	-- callbacks
	stage:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	stage:addEventListener( Event.ENTER_FRAME, self.Event_FrameUpdate, self )
end

function BoardgameState:Teardown()
	NG_DEBUG:LOG( "BoardgameState:Teardown", "info", "Function begin" )
	NG_RENDERER:CLEAR_DRAWABLES()
end

function BoardgameState:Update()
end

function BoardgameState:Event_AndroidKey( event )
	NG_DEBUG:LOG( "BoardgameState:Event_AndroidKey", "info", "Function begin" )
end

function BoardgameState:Event_MouseClick( event )
	NG_DEBUG:LOG( "BoardgameState:Event_MouseClick", "info", "Function begin" )
	
	if ( self.waitingEvent == "enemy dead" or self.waitingEvent == "end battle" ) then
		return
	end
	
	if ( self.view == "player-move" ) then
		self:CheckPlayerInput_Move( event )
	elseif ( self.view == "player-battle" ) then
		self:CheckPlayerInput_Battle( event )
	end
end

function BoardgameState:Event_FrameUpdate()
	if ( self.animationTimer ~= nil and self.animationTimer > 0 ) then
		self.animationTimer = self.animationTimer - 1
		
		if ( self.animationTimer == 0 ) then
			self:ClearAnimatedStatus()
		end
	end
	
	self:CheckForWaitingEvent()
end

function BoardgameState:RotateView( angle )
	NG_DEBUG:LOG( "BoardgameState:RotateView", "info", "Angle: " .. angle )
	
		self.viewport:setAnchorPosition( 720/2, 1280/2 )
		self.viewport:setPosition( 720/2, 1280/2 )
	
	if ( angle == 90 ) then
		self.viewport:setRotation( angle )
	else
		self.viewport:setRotation( angle )
	end
		
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

function BoardgameState:ClearHighlights()
	NG_DEBUG:LOG( "BoardgameState:ClearHighlights", "info", "Function begin, size of self.highlights: " .. #self.highlights )
	-- Clear out highlights
	for key, highlight in pairs( self.highlights ) do
		highlight["button"]:Cleanup()
	end
	self.highlights = {}
	
	NG_DEBUG:LOG( "BoardgameState:ClearHighlights", "info", "Function end, size of self.highlights: " .. #self.highlights )
end

function BoardgameState:HighlightMovableTiles()
	NG_DEBUG:LOG( "BoardgameState:HighlightMovableTiles", "info", "Function begin" )
	self:ClearHighlights()
	
	-- New highlights
	local alreadyVisited = {}
	self:HighlightMovableTiles_Recursive( 1, alreadyVisited )
	
	NG_RENDERER:DRAW()
end

function BoardgameState:HighlightMovableTiles_Recursive( currentTile, alreadyVisited )
	-- Don't double-traverse
	if ( alreadyVisited[ currentTile ] ~= nil ) then
		return
	end
	
	alreadyVisited[ currentTile ] = true

	-- Highlight self
	local tile = self.boardPath[ currentTile ]
	
	local texture = "marker"
	if ( tile["enemy"] ~= nil ) then
		texture = "red-marker"
	end
	
	tx, ty = self:GetAdjustedCoordinates( tile["tilex"], tile["tiley"], self.tileWidthHeight )
	local highlightTile = ngButton.new( { drawSurface = NG_RENDERER:LAYER( 2 ), x = tx, y = ty, background = NG_ASSET_MANAGER:GET_TEXTURE( texture ), style = "blink" } )
	self.highlights[ currentTile ] = { enemy = tile["enemy"], tilex = tile["tilex"], tiley = tile["tiley"], button = highlightTile }
	NG_RENDERER:ADD_DRAWABLES( currentTile, highlightTile )
	
	local neighbors = {}
	if ( tile["neighbor_down"] ~= nil ) then table.insert( neighbors, tile["neighbor_down"] ) end
	if ( tile["neighbor_up"] ~= nil ) then table.insert( neighbors, tile["neighbor_up"] ) end
	if ( tile["neighbor_left"] ~= nil ) then table.insert( neighbors, tile["neighbor_left"] ) end
	if ( tile["neighbor_right"] ~= nil ) then table.insert( neighbors, tile["neighbor_right"] ) end
	
	if ( tile["enemy"] ~= nil ) then
		return
	end
	
	for key, neighbor in pairs( neighbors ) do
		self:HighlightMovableTiles_Recursive( neighbor, alreadyVisited )
	end
end

-- Ugly init code

function BoardgameState:RegisterAssets()
	
	local p1Avatar = self.playerOptions[1][ "avatar" ]
	local p2Avatar = self.playerOptions[2][ "avatar" ]
	local p3Avatar = self.playerOptions[3][ "avatar" ]
	local p4Avatar = self.playerOptions[4][ "avatar" ]
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg", path = "content/graphics/ui/default-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "board", path = "content/graphics/board/board1.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "overlay", path = "content/graphics/board/overlay.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "dark-bg", path = "content/graphics/ui/dark-bg.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-bg", path = "content/graphics/ui/button-med.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "marker", path = "content/graphics/ui/marker.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "red-marker", path = "content/graphics/ui/redmarker.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "heart", path = "content/graphics/ui/heart.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "dialog-bg", path = "content/graphics/ui/dialog.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "scroll-up", path = "content/graphics/ui/scroll-up.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "scroll-down", path = "content/graphics/ui/scroll-down.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "battle-bg", path = "content/graphics/ui/battle-bg.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "number-entry", path = "content/graphics/ui/number-entry.png" } )
	
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "main", size = 60, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "big", size = 100, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "sub", size = 30, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "debug", size = 15, path = "content/fonts/Sniglet-Regular.ttf" } )
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-1", path = "content/graphics/avatars/avatar-" .. p1Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-2", path = "content/graphics/avatars/avatar-" .. p2Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-3", path = "content/graphics/avatars/avatar-" .. p3Avatar .. ".png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-4", path = "content/graphics/avatars/avatar-" .. p4Avatar .. ".png" } )
	
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "mosquito", path = "content/graphics/avatars/mosquito.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "mosquito-king", path = "content/graphics/avatars/mosquito-king.png" } )
	
	NG_ASSET_MANAGER:REGISTER_SOUND( { key = "wrong-answer", path = "content/audio/wrong-answer.wav", isMusic = false } )
	NG_ASSET_MANAGER:REGISTER_SOUND( { key = "right-answer", path = "content/audio/right-answer.wav", isMusic = false } )
	NG_ASSET_MANAGER:REGISTER_SOUND( { key = "enemy-dead", path = "content/audio/enemy-dead.wav", isMusic = false } )
	NG_ASSET_MANAGER:REGISTER_SOUND( { key = "button-pressed", path = "content/audio/button-press.wav", isMusic = false } )
end

function BoardgameState:CreateBattleDrawables()
	
	local scrollerTop = 400 + self.screenTopY
	local numberView = scrollerTop + 75
	local scrollerDown = numberView + 147
	
	NG_RENDERER:ADD_DRAWABLES( "dark-bg", ngImage.new( 	{ drawSurface = NG_RENDERER:LAYER( 1 ), x = 0, y = 0, background = NG_ASSET_MANAGER:GET_TEXTURE( "dark-bg" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "dialog", ngImage.new(		{ drawSurface = NG_RENDERER:LAYER( 2 ), x = 0, y = self.screenTopY, background = NG_ASSET_MANAGER:GET_TEXTURE( "dialog-bg" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "battle-bg", ngImage.new(	{ drawSurface = NG_RENDERER:LAYER( 3 ), x = 22, y = self.screenTopY + 22, background = NG_ASSET_MANAGER:GET_TEXTURE( "battle-bg" ) } ) )

	NG_RENDERER:ADD_DRAWABLES( "math-problem",  ngLabel.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 20, y = self.screenTopY + 575, color = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "big" ), text = self.questionNum1 .. " " .. self.questionOp .. " " .. self.questionNum2 .. " = " } ) )
	NG_RENDERER:ADD_DRAWABLES( "status",  ngLabel.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 250, y = self.screenTopY + 100, color = 0xffff00, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = "" } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "hundreds-up", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 350, y = scrollerTop, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-up" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "tens-up", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 475, y = scrollerTop, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-up" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "ones-up", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 600, y = scrollerTop, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-up" ) } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "digit-hundreds", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 350, y = numberView, textColor = 0xffffff, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = self.hundreds, background = NG_ASSET_MANAGER:GET_TEXTURE( "number-entry" ), textX = 30, textY = 90 } ) )
	NG_RENDERER:ADD_DRAWABLES( "digit-tens", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 475, y = numberView, textColor = 0xffffff, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = self.tens, background = NG_ASSET_MANAGER:GET_TEXTURE( "number-entry" ), textX = 30, textY = 90 } ) )
	NG_RENDERER:ADD_DRAWABLES( "digit-ones", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 600, y = numberView, textColor = 0xffffff, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = self.ones, background = NG_ASSET_MANAGER:GET_TEXTURE( "number-entry" ), textX = 30, textY = 90 } ) )

	NG_RENDERER:ADD_DRAWABLES( "hundreds-down", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 350, y = scrollerDown, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-down" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "tens-down", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 475, y = scrollerDown, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-down" ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "ones-down", ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 600, y = scrollerDown, background = NG_ASSET_MANAGER:GET_TEXTURE( "scroll-down" ) } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "submit-answer",  ngButton.new( { drawSurface = NG_RENDERER:LAYER( 3 ), x = 50, y = self.screenTopY + 625, font = NG_ASSET_MANAGER:GET_FONT( "main" ), text = "OK", textColor = 0x000000, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), textX = 60, textY = 60 } ) )
	
	NG_RENDERER:ADD_DRAWABLES( "player-battle",  ngImage.new( 	{ drawSurface = NG_RENDERER:LAYER( 4 ), x = 50, y = 450, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-" .. self.currentTurn ) } ) )
	NG_RENDERER:ADD_DRAWABLES( "enemy-battle",  ngImage.new( 	{ drawSurface = NG_RENDERER:LAYER( 4 ), x = 500, y = 450, background = NG_ASSET_MANAGER:GET_TEXTURE( self.boardPath[ self.currentTile ]["enemy"] ) } ) )
end