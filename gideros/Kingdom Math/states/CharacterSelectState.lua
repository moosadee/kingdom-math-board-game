CharacterSelectState = Core.class()

function CharacterSelectState:RotateView( angle )
	NG_DEBUG:LOG( "CharacterSelectState:RotateView", "info", "Angle: " .. angle )
	
	self.viewport:setAnchorPosition( 720/2, 1280/2 )
	self.viewport:setPosition( 720/2, 1280/2 )
	
	if ( angle == 90 ) then
		self.viewport:setRotation( angle )
	else
		self.viewport:setRotation( angle )
	end
		
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

function CharacterSelectState:GotoNextState()
	NG_CONFIG_MANAGER:SET( "playerOptions", self.playerOptions )
	self.gotoState = "BOARDGAME"
	self:Setup_ConfirmTeardown()
end

-- Functions that all states should have

function CharacterSelectState:init( options )
	NG_DEBUG:LOG( "CharacterSelectState:init", "info", "State key \"" .. options.name .. "\"" )
	self.buttons = {}
	self.labels = {}
	self.name = options.name
	self.view = ""
	self.totalPlayers = 0
	self.currentPlayerSetup = 0
	self.screenTopY = (1280 - 720) / 2
	self.screenWidthHeight = 720
	self.viewAngle = 0
	self.viewport = Viewport.new()
	
	self.gotoState = ""

	stage:addChild( self.viewport )
	
	self.playerOptions = {
		{ name = "PLAYER 1", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 0, 	avatar = 1, totalCorrect = 0, questionsAnswered = 0 },
		{ name = "PLAYER 2", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 180, 	avatar = 1, totalCorrect = 0, questionsAnswered = 0 },
		{ name = "PLAYER 3", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 90, 	avatar = 1, totalCorrect = 0, questionsAnswered = 0 },
		{ name = "PLAYER 4", 	enableAddition = true, enableSubtraction = true, enableMultiplication = true, enableDivision = true, angle = 270, 	avatar = 1, totalCorrect = 0, questionsAnswered = 0 }
	}
end

function CharacterSelectState:GotoState()
	return self.gotoState
end

function CharacterSelectState:Start()
	self:Setup()
end

function CharacterSelectState:Stop()
	self.Teardown()
end

function CharacterSelectState:Setup()
	NG_DEBUG:LOG( "CharacterSelectState:Setup", "info", "Function begin" )
	
	self.view = "how-many-players"
	
	self:RegisterAssets()

	self.background = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg" ) )
	self.subBackground = Bitmap.new( NG_ASSET_MANAGER:GET_TEXTURE( "default-bg-sub" ) )
	self.subBackground:setPosition( 0, (1280-720)/2 )
	
	NG_ASSET_MANAGER:REGISTER_SOUND( { key = "button-pressed", path = "content/audio/button-press.wav", isMusic = false } )

	self.buttons = {}
	self.labels = {}
	self.images = {}
	
	self.gotoState = ""
	
	-- callbacks
	stage:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	
	-- Set up screen
	self:Setup_HowManyPlayersScreen()
end

function CharacterSelectState:Teardown()
	NG_DEBUG:LOG( "CharacterSelectState:Teardown", "info", "Function begin" )
end

function CharacterSelectState:Update()
end

function CharacterSelectState:Event_AndroidKey( event )
	NG_DEBUG:LOG( "CharacterSelectState:Event_AndroidKey", "info", "Function begin" )
end

function CharacterSelectState:Event_MouseClick( event )
	NG_DEBUG:LOG( "CharacterSelectState:Event_MouseClick", "info", "Function begin" )
	
	-- How many players? screen
	if ( self.view == "how-many-players" ) then
		self.view = "player-setup"
		if ( self.buttons["one"]:IsClicked( event ) ) then
			self.totalPlayers = 1
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["two"]:IsClicked( event ) ) then
			self.totalPlayers = 2
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["three"]:IsClicked( event ) ) then
			self.totalPlayers = 3
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["four"]:IsClicked( event ) ) then
			self.totalPlayers = 4
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		else
			self.view = "how-many-players"
		end
		
		if ( self.view == "player-setup" ) then
			self.currentPlayerSetup = 1
			self:Teardown_HowManyPlayersScreen()
			self:Setup_PlayerSetup()
		end
	elseif ( self.view == "player-setup" ) then
		if ( self.buttons["avatar-1"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 1
			x, y, z = self.buttons["avatar-1"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["avatar-2"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 2
			x, y, z = self.buttons["avatar-2"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["avatar-3"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 3
			x, y, z = self.buttons["avatar-3"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["avatar-4"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 4
			x, y, z = self.buttons["avatar-4"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["avatar-5"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 5
			x, y, z = self.buttons["avatar-5"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
		elseif ( self.buttons["avatar-6"]:IsClicked( event ) ) then
			self.playerOptions[ self.currentPlayerSetup ].avatar = 6
			x, y, z = self.buttons["avatar-6"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
		end
		
		-- Enable/disable addition
		if ( self.buttons["addition"]:IsClicked( event ) ) then
			if ( self.playerOptions[ self.currentPlayerSetup ].enableAddition == true ) then		-- Disable addition
				self.buttons["addition"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-off" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableAddition = false
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			else															-- Enable addition
				self.buttons["addition"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableAddition = true
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			end
		end
		
		-- Enable/disable subtraction
		if ( self.buttons["subtraction"]:IsClicked( event ) ) then
			if ( self.playerOptions[ self.currentPlayerSetup ].enableSubtraction == true ) then		-- Disable addition
				self.buttons["subtraction"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-off" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableSubtraction = false
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			else															-- Enable addition
				self.buttons["subtraction"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableSubtraction = true
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			end
		end
		
		-- Enable/disable multiplication
		if ( self.buttons["multiplication"]:IsClicked( event ) ) then
			if ( self.playerOptions[ self.currentPlayerSetup ].enableMultiplication == true ) then		-- Disable addition
				self.buttons["multiplication"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-off" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableMultiplication = false
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			else															-- Enable addition
				self.buttons["multiplication"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableMultiplication = true
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			end
		end
		
		-- Enable/disable division
		if ( self.buttons["division"]:IsClicked( event ) ) then
			if ( self.playerOptions[ self.currentPlayerSetup ].enableDivision == true ) then		-- Disable addition
				self.buttons["division"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-off" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableDivision = false
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			else															-- Enable addition
				self.buttons["division"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
				self.playerOptions[ self.currentPlayerSetup ].enableDivision = true
				NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			end
		end
		
		-- Change player name
		if ( self.buttons["name"]:IsClicked( event ) ) then
			local textInput = TextInputDialog.new( "Player " .. self.currentPlayerSetup, "Enter your name", "", "Cancel", "OK" )
			
			local function onComplete( event )
				-- print( event.text, event.buttonIndex, event.buttonText )
				if ( event.buttonText == "OK" ) then
					self.buttons["name"]:ChangeText( { text = event.text, textX = 100, textY = 75 } )
					self.playerOptions[ self.currentPlayerSetup ].name = event.text
				end
			end
			
			textInput:addEventListener( Event.COMPLETE, onComplete )
			textInput:show()
		end
		
		-- Next
		if ( self.buttons["next"]:IsClicked( event ) ) then
			self.currentPlayerSetup = self.currentPlayerSetup + 1
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			
			-- Reset selector
			x, y, z = self.buttons["avatar-1"]:GetPosition()
			self.images["selector"]:SetPosition( x, y )
			
			-- Reset math options
			self.buttons["addition"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
			self.buttons["subtraction"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
			self.buttons["multiplication"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
			self.buttons["division"]:ChangeBackground( { background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ) } )
			
			-- Update field text
			self.labels["player-header"]:SetText( "Player " .. self.currentPlayerSetup )
			self.buttons["name"]:ChangeText( { text = "NAME", textX = 100, textY = 75 } )
			
			if ( self.currentPlayerSetup > self.totalPlayers ) then
				-- Done with character setup
				self.view = "confirm"
				self:Setup_PlayerTeardown()
				self:Setup_ConfirmSetup()
				--self:RotateView( self.playerOptions[ 1 ].angle )
			else
				--self:RotateView( self.playerOptions[ self.currentPlayerSetup ].angle )
			end
		end
	
	elseif ( self.view == "confirm" ) then
		if ( self.buttons["play"]:IsClicked( event ) ) then
			NG_ASSET_MANAGER:PLAY_SOUND( "button-pressed" )
			self:GotoNextState()
		end
	end
end

function CharacterSelectState:Event_FrameUpdate()
end

-- Ugly init code

function CharacterSelectState:Setup_HowManyPlayersScreen()
	NG_DEBUG:LOG( "CharacterSelectState:Setup_HowManyPlayersScreen", "info", "Function begin" )
	
	self.labels["header"] = ngLabel.new( { drawSurface = self.viewport, x = 150, y = self.screenTopY + 50, text = "How many players?", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0xffffff } )
	
	local tx = 75
	local ty = 140
	self.buttons["one"] = ngButton.new( {
		background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), x = 125, y = self.screenTopY + 100,
		drawSurface = self.viewport, text = "1", font = NG_ASSET_MANAGER:GET_FONT( "bigbutton" ), textY = ty, textX = tx
		} )
		
	self.buttons["two"] = ngButton.new( {
		background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), x = 425, y = self.screenTopY + 100,
		drawSurface = self.viewport, text = "2", font = NG_ASSET_MANAGER:GET_FONT( "bigbutton" ), textY = ty, textX = tx
		} )
		
	self.buttons["three"] = ngButton.new( {
		background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), x = 125, y = self.screenTopY + 400,
		drawSurface = self.viewport, text = "3", font = NG_ASSET_MANAGER:GET_FONT( "bigbutton" ), textY = ty, textX = tx
		} )
		
	self.buttons["four"] = ngButton.new( {
		background = NG_ASSET_MANAGER:GET_TEXTURE( "button-bg" ), x = 425, y = self.screenTopY + 400,
		drawSurface = self.viewport, text = "4", font = NG_ASSET_MANAGER:GET_FONT( "bigbutton" ), textY = ty, textX = tx
		} )
	
	self.viewport:addChild( self.subBackground )
	
	for key, button in pairs( self.buttons ) do
		button:Draw( self.viewport )
	end
	
	for key, label in pairs( self.labels ) do
		label:Draw()
	end
	
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

function CharacterSelectState:Teardown_HowManyPlayersScreen()
	NG_DEBUG:LOG( "CharacterSelectState:Teardown_HowManyPlayersScreen", "info", "Function begin" )
	
	for key, button in pairs( self.buttons ) do
		button:Cleanup()
	end
	
	for key, label in pairs( self.labels ) do
		label:Cleanup()
	end
	
	self.buttons = {}
	self.labels = {}
end

function CharacterSelectState:Setup_PlayerSetup()
	NG_DEBUG:LOG( "CharacterSelectState:Setup_PlayerSetup", "info", "Function begin" )
	
	self.labels["player-header"] = ngLabel.new( { drawSurface = self.viewport, x = 70, y = self.screenTopY + 80, text = "Player " .. self.currentPlayerSetup, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0xffe177 } )
	self.labels["player-header"]:SetRotation( 4 )
	self.labels["avatar-header"] = ngLabel.new( { drawSurface = self.viewport, x = 60, y = self.screenTopY + 330, text = "Avatar", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0xffffff } )
	self.labels["avatar-header"]:SetRotation( -90 )
	self.labels["level-header"] = ngLabel.new( { drawSurface = self.viewport, x = 60, y = self.screenTopY + 600, text = "Level", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0xffffff } )
	self.labels["level-header"]:SetRotation( -90 )
	--self.labels["name-header"] = ngLabel.new( { x = 10, y = self.screenTopY + 650, text = "Name:", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
	
	spacing = (self.screenWidthHeight - 150) / 3
	
	local scaleVal = 0.5
	-- Avatar selection
	self.buttons["avatar-1"] = ngButton.new( { drawSurface = self.viewport, x = spacing*0+150, y = self.screenTopY + 145, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-1" ), scale = scaleVal } )
	self.buttons["avatar-2"] = ngButton.new( { drawSurface = self.viewport, x = spacing*1+150, y = self.screenTopY + 145, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-2" ), scale = scaleVal } )
	self.buttons["avatar-3"] = ngButton.new( { drawSurface = self.viewport, x = spacing*2+150, y = self.screenTopY + 145, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-3" ), scale = scaleVal } )
	self.buttons["avatar-4"] = ngButton.new( {drawSurface = self.viewport, x = spacing*0+150, y = self.screenTopY + 245, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-4" ), scale = scaleVal } )
	self.buttons["avatar-5"] = ngButton.new( {drawSurface = self.viewport, x = spacing*1+150, y = self.screenTopY + 245, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-5" ), scale = scaleVal } )
	self.buttons["avatar-6"] = ngButton.new( {drawSurface = self.viewport, x = spacing*2+150, y = self.screenTopY + 245, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-6" ), scale = scaleVal } )
	
	self.images["selector"] = ngImage.new( { drawSurface = self.viewport, x = spacing*0+150, y = self.screenTopY + 145, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-selector" ), scaleX = scaleVal, scaleY = 0.5 } )
	
	spacing = (self.screenWidthHeight - 150) / 4
	
	-- Math level selection		button-switch-on
	self.buttons["addition"] 		= ngButton.new( { drawSurface = self.viewport, x = spacing*0+130, y = self.screenTopY + 475, text = "+", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "main" ), textX = 40, textY = 60, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ), scale = scaleVal } )
	self.buttons["subtraction"] 	= ngButton.new( { drawSurface = self.viewport, x = spacing*1+130, y = self.screenTopY + 475, text = "-", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "main" ), textX = 40, textY = 60, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ), scale = scaleVal } )
	self.buttons["multiplication"] = ngButton.new( { drawSurface = self.viewport, x = spacing*2+130, y = self.screenTopY + 475, text = "x", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "main" ), textX = 40, textY = 60, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ), scale = scaleVal } )
	self.buttons["division"] 			= ngButton.new( { drawSurface = self.viewport, x = spacing*3+130, y = self.screenTopY + 475, text = "÷", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "main" ), textX = 40, textY = 60, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-switch-on" ), scale = scaleVal } )
	
	-- Name change buttons
	self.buttons["name"] 			= ngButton.new( { drawSurface = self.viewport, x = 250, y = self.screenTopY + 40, text = "NAME", textColor = 0xffe177, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), textX = 150, textY = 60, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-long" ) } )
	--self.buttons["name"]:SetRotation(-1)
	
	-- Continue button
	self.buttons["next"] 			= ngButton.new( { drawSurface = self.viewport, x = 500, y = self.screenTopY + 630, text = "Next >", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), textX = 40, textY = 55, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-med" ) } )
	
	self.subBackground:setTexture( NG_ASSET_MANAGER:GET_TEXTURE( "charselect-bg" ) )
	self.viewport:addChild( self.subBackground )
	
	for key, image in pairs( self.images ) do
		image:Draw()
	end
	
	for key, label in pairs( self.labels ) do
		label:Draw()
	end
	
	for key, button in pairs( self.buttons ) do
		button:Draw()
	end
	
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

function CharacterSelectState:Setup_PlayerTeardown()
	NG_DEBUG:LOG( "CharacterSelectState:Setup_PlayerTeardown", "info", "Function begin" )
	for key, label in pairs( self.labels ) do
		label:Cleanup()
	end
	
	for key, button in pairs( self.buttons ) do
		button:Cleanup()
	end
	
	for key, image in pairs( self.images ) do
		image:Cleanup()
	end
	
	self.labels = {}
	self.buttons = {}
	self.images = {}
end

function CharacterSelectState:Setup_ConfirmSetup()
	self.labels["header"] = ngLabel.new( { drawSurface = self.viewport, x = 230, y = self.screenTopY + 95, text = "All ready?", font = NG_ASSET_MANAGER:GET_FONT( "main" ), color = 0xffe177 } )
	
	self.labels["player1"] = ngLabel.new( { drawSurface = self.viewport, x = 50, y = self.screenTopY + 320, text = "Player 1, ", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
	self.labels["player1b"] = ngLabel.new( { drawSurface = self.viewport, x = 50, y = self.screenTopY + 370, text = self.playerOptions[ 1 ].name, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
	self.buttons["player1av"] = ngButton.new( { drawSurface = self.viewport, x = 100, y = self.screenTopY + 175, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-" .. self.playerOptions[ 1 ].avatar ), scale = 0.60 } )
	
	if ( self.totalPlayers >= 2 ) then
		self.labels["player2"] = ngLabel.new( { drawSurface = self.viewport, x = 390, y = self.screenTopY + 320, text = "Player 2, ", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.labels["player2b"] = ngLabel.new( { drawSurface = self.viewport, x = 390, y = self.screenTopY + 370, text = self.playerOptions[ 2 ].name, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.buttons["player2av"] = ngButton.new( { drawSurface = self.viewport, x = 450, y = self.screenTopY + 175, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-" .. self.playerOptions[ 2 ].avatar ), scale = 0.60 } )
	end
	
	if ( self.totalPlayers >= 3 ) then
		self.labels["player3"] = ngLabel.new( { drawSurface = self.viewport, x = 85, y = self.screenTopY + 570, text = "Player 3, ", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.labels["player3b"] = ngLabel.new( { drawSurface = self.viewport, x = 85, y = self.screenTopY + 620, text = self.playerOptions[ 3 ].name, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.buttons["player3av"] = ngButton.new( { drawSurface = self.viewport, x = 130, y = self.screenTopY + 420, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-" .. self.playerOptions[ 3 ].avatar ), scale = 0.60 } )
	end
	
	if ( self.totalPlayers >= 4 ) then
		self.labels["player4"] = ngLabel.new( { drawSurface = self.viewport, x = 420, y = self.screenTopY + 570, text = "Player 4, ", font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.labels["player4b"] = ngLabel.new( { drawSurface = self.viewport, x = 420, y = self.screenTopY + 620, text = self.playerOptions[ 4 ].name, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), color = 0x000000 } )
		self.buttons["player4av"] = ngButton.new( { drawSurface = self.viewport, x = 470, y = self.screenTopY + 420, background = NG_ASSET_MANAGER:GET_TEXTURE( "avatar-" .. self.playerOptions[ 4 ].avatar ), scale = 0.60 } )
	end
	
	self.buttons["play"] = ngButton.new( { drawSurface = self.viewport, x = 500, y = self.screenTopY + 630, text = "Play!", textColor = 0x000000, font = NG_ASSET_MANAGER:GET_FONT( "header2" ), textX = 40, textY = 55, background = NG_ASSET_MANAGER:GET_TEXTURE( "button-med" ) } )
	
	self.subBackground:setTexture( NG_ASSET_MANAGER:GET_TEXTURE( "confirm-bg" ) )
	self.viewport:addChild( self.subBackground )
	
	for key, label in pairs( self.labels ) do
		label:Draw()
	end
	
	for key, button in pairs( self.buttons ) do
		button:Draw()
	end
	
	for key, image in pairs( self.images ) do
		image:Draw()
	end
	
	stage:addChild( self.background )
	stage:addChild( self.viewport )
end

function CharacterSelectState:Setup_ConfirmTeardown()
	NG_DEBUG:LOG( "CharacterSelectState:Setup_ConfirmTeardown", "info", "Function begin" )
	for key, label in pairs( self.labels ) do
		label:Cleanup()
	end
	
	for key, button in pairs( self.buttons ) do
		button:Cleanup()
	end
	
	for key, image in pairs( self.images ) do
		image:Cleanup()
	end
end

function CharacterSelectState:RegisterAssets()
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg", path = "content/graphics/ui/default-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "default-bg-sub", path = "content/graphics/ui/default-background-sub.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "charselect-bg", path = "content/graphics/ui/charselect-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "confirm-bg", path = "content/graphics/ui/confirm-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-bg", path = "content/graphics/ui/button-background.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-long", path = "content/graphics/ui/button-long.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-med", path = "content/graphics/ui/button-med.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-selector", path = "content/graphics/ui/avatar-selector.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-switch-on", path = "content/graphics/ui/button-switch-on.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "button-switch-off", path = "content/graphics/ui/button-switch-off.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-1", path = "content/graphics/avatars/avatar-head-1.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-2", path = "content/graphics/avatars/avatar-head-2.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-3", path = "content/graphics/avatars/avatar-head-3.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-4", path = "content/graphics/avatars/avatar-head-4.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-5", path = "content/graphics/avatars/avatar-head-5.png" } )
	NG_ASSET_MANAGER:REGISTER_TEXTURE( { key = "avatar-6", path = "content/graphics/avatars/avatar-head-6.png" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "bigbutton", size = 100, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "main", size = 60, path = "content/fonts/Sniglet-Regular.ttf" } )
	NG_ASSET_MANAGER:REGISTER_FONT( { key = "header2", size = 50, path = "content/fonts/Sniglet-Regular.ttf" } )
end