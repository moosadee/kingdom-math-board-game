package com.moosader.mathkingdomquest

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun gotoOptions(view: View) {
        val changePage = Intent( this, OptionsActivity::class.java )
        startActivity(changePage)
    }

    fun gotoHelp(view: View) {
        val changePage = Intent( this, HelpActivity::class.java )
        startActivity(changePage)
    }

    fun gotoHeroHub(view: View) {
        val changePage = Intent( this, HeroHubActivity::class.java )
        startActivity(changePage)
    }
}
